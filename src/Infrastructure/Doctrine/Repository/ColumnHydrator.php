<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository;

use Doctrine\ORM\Internal\Hydration\AbstractHydrator;
use PDO;

/**
 * Гидратор для получение одномерного скалярного массива
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class ColumnHydrator extends AbstractHydrator
{
    public const TYPE = 'Column';

    /**
     * @inheritDoc
     */
    protected function hydrateAllData(): array
    {
        return $this->_stmt->fetchAll(PDO::FETCH_COLUMN);
    }
}
