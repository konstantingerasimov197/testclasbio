<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\EntityManager;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Класс-обёртка над транзациями бд
 *
 * @author Oleg Maksimenko <ya@olegpro.ru>
 */
class Transaction
{
    private EntityManagerInterface $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Старт транзакции
     */
    public function begin(): void
    {
        $this->entityManager->beginTransaction();
    }

    /**
     * Коммит транзакции
     */
    public function commit(): void
    {
        $this->entityManager->commit();
    }

    /**
     * Отмена транзакции
     */
    public function rollback(): void
    {
        $this->entityManager->rollback();
    }

    /**
     * Обоарчивает исполняемый код в транзакцию
     *
     * @param callable $func
     */
    public function wrap(callable $func): void
    {
        $this->entityManager->transactional($func);
    }
}
