<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\EntityManager;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Флушер изменений UOW
 *
 * @author Oleg Maksimenko <ya@olegpro.ru>
 */
class Flusher
{
    private EntityManagerInterface $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Сохранение изменений
     */
    public function flush(): void
    {
        $this->entityManager->flush();
    }
}
