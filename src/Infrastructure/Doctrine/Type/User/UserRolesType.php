<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Type\User;

use App\Model\User\Enum\UserRoleEnum;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserRolesType extends JsonType
{
    /**
     * @inheritdoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        $fieldDeclaration['jsonb'] = true;
        return $platform->getJsonTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @inheritdoc
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var UserRoleEnum[] $value */
        $roles = [];

        foreach ($value as $role) {
            $roles[] = $role->getValue();
        }

        return json_encode($roles, JSON_THROW_ON_ERROR, 512);
    }

    /**
     * @inheritdoc
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $rawValues = json_decode($value, true, 512, JSON_THROW_ON_ERROR);
        $roles = [];

        foreach ($rawValues as $rawValue) {
            $roles[] = UserRoleEnum::get($rawValue);
        }

        return $roles;
    }

    /**
     * @inheritDoc
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
