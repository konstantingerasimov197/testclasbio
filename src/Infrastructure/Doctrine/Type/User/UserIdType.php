<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Type\User;

use App\Infrastructure\Doctrine\Type\BaseUuidType;
use App\Model\User\Value\UserId;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserIdType extends BaseUuidType
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'user_id';
    }

    /**
     * @inheritDoc
     */
    protected function getIdentifierClass(): string
    {
        return UserId::class;
    }
}
