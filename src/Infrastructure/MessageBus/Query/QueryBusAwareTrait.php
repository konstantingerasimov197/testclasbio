<?php

declare(strict_types=1);

namespace App\Infrastructure\MessageBus\Query;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
trait QueryBusAwareTrait
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @param QueryCommandInterface $query
     *
     * @return QueryResultInterface
     */
    public function handleQuery(QueryCommandInterface $query): QueryResultInterface
    {
        return $this->queryBus->query($query);
    }

    /**
     * @param QueryBus $queryBus
     */
    public function setQueryBus(QueryBus $queryBus): void
    {
        $this->queryBus = $queryBus;
    }
}
