<?php

declare(strict_types=1);

namespace App\Infrastructure\MessageBus\Query;

use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class QueryBus
{
    use HandleTrait;

    /**
     * @param MessageBusInterface $queryBus
     */
    public function __construct(MessageBusInterface $queryBus)
    {
        $this->messageBus = $queryBus;
    }

    /**
     * @param QueryCommandInterface $query
     *
     * @return QueryResultInterface
     */
    public function query(QueryCommandInterface $query): QueryResultInterface
    {
        return $this->handle($query);
    }
}
