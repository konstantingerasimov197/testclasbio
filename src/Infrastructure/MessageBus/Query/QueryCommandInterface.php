<?php

declare(strict_types=1);

namespace App\Infrastructure\MessageBus\Query;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
interface QueryCommandInterface
{
}
