<?php

declare(strict_types=1);

namespace App\Infrastructure\MessageBus\Query;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
interface QueryBusAwareInterface
{
    /**
     * @param QueryCommandInterface $query
     *
     * @return QueryResultInterface
     */
    public function handleQuery(QueryCommandInterface $query): QueryResultInterface;

    /**
     * @param QueryBus $queryBus
     */
    public function setQueryBus(QueryBus $queryBus): void;
}
