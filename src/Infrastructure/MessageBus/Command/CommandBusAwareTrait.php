<?php

declare(strict_types=1);

namespace App\Infrastructure\MessageBus\Command;

use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @author Ilya Grishenkov <grishenkov.i@book24.ru>
 */
trait CommandBusAwareTrait
{
    /**
     * @var MessageBusInterface
     */
    private $commandBus;

    /**
     * @param CommandInterface $command
     */
    public function handleCommand(CommandInterface $command): void
    {
        $this->commandBus->dispatch($command);
    }

    /**
     * @param MessageBusInterface $commandBus
     */
    public function setCommandBus(MessageBusInterface $commandBus): void
    {
        $this->commandBus = $commandBus;
    }
}
