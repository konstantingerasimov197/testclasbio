<?php

declare(strict_types=1);

namespace App\Infrastructure\MessageBus\Command;

/**
 * Интерфейс для команд
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
interface CommandInterface
{
}
