<?php

declare(strict_types=1);

namespace App\Infrastructure\MessageBus;

use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
interface ValidableMessageInterface
{
    /**
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata): void;
}
