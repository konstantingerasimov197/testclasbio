<?php

declare(strict_types=1);

namespace App\Infrastructure\MessageBus\Middleware;

use App\Infrastructure\MessageBus\ValidableMessageInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class ValidationMiddleware implements MiddlewareInterface
{
    private ValidatorInterface $validator;

    /**
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @inheritDoc
     */
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        $message = $envelope->getMessage();

        if ($message instanceof ValidableMessageInterface) {
            $violations = $this->validator->validate($message);

            if (count($violations) > 0) {
                throw new ValidationFailedException($message, $violations);
            }
        }

        return $stack->next()->handle($envelope, $stack);
    }
}
