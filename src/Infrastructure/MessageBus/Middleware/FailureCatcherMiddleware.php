<?php

declare(strict_types=1);

namespace App\Infrastructure\MessageBus\Middleware;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Throwable;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class FailureCatcherMiddleware implements MiddlewareInterface
{
    /**
     * @inheritDoc
     *
     * @throws Throwable
     */
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        try {
            $returnedEnvelope = $stack->next()->handle($envelope, $stack);
        } catch (HandlerFailedException $exception) {
            throw $exception->getNestedExceptions()[0];
        }

        return $returnedEnvelope;
    }
}
