<?php

declare(strict_types=1);

namespace App\Model\User\Validation;

use Closure;
use Symfony\Component\Validator\Constraint;

/**
 * Проверка на уникальность Email пользователя
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserEmailUnique extends Constraint
{
    public const ERROR_CODE = '11781d2c-6a3d-4b51-ad58-2959f316b40f';

    public string $message = 'Email уже используется.';
    public ?Closure $getIdCallback = null;
}
