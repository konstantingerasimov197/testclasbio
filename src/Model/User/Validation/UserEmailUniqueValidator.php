<?php

declare(strict_types=1);

namespace App\Model\User\Validation;

use App\Model\User\Repository\Filter\UserByEmailFilter;
use App\Model\User\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserEmailUniqueValidator extends ConstraintValidator
{
    private UserRepository $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UserEmailUnique) {
            throw new UnexpectedTypeException($constraint, UserEmailUnique::class);
        }

        if (!is_string($value)) {
            return;
        }

        $filter = new UserByEmailFilter($value);
        $user = $this->repository->findByFilter($filter);

        if ($user !== null) {
            if ($constraint->getIdCallback !== null) {
                $callback = $constraint->getIdCallback;
                $userId = $callback($this->context->getRoot());

                if ($user->getId()->equals($userId)) {
                    return;
                }
            }

            $this->context
                ->buildViolation($constraint->message)
                ->setCode(UserEmailUnique::ERROR_CODE)
                ->addViolation();
        }
    }
}
