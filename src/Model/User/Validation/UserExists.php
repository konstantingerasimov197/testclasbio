<?php

declare(strict_types=1);

namespace App\Model\User\Validation;

use Symfony\Component\Validator\Constraint;

/**
 * Проверка на существование пользователя по идентификатору
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserExists extends Constraint
{
    public const CODE = '5722c6b7-f708-4242-9559-d617bbed4b7b';

    public string $message = 'Пользователя не существует.';
}
