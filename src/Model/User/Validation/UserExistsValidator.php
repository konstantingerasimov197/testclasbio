<?php

declare(strict_types=1);

namespace App\Model\User\Validation;

use App\Model\User\Repository\UserRepository;
use App\Model\User\Value\UserId;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserExistsValidator extends ConstraintValidator
{
    private UserRepository $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UserExists) {
            throw new UnexpectedTypeException($constraint, UserExists::class);
        }

        if (!$value instanceof UserId) {
            return;
        }

        $exists = $this->repository->existsById($value);

        if (!$exists) {
            $this->context
                ->buildViolation($constraint->message)
                ->setCode(UserExists::CODE)
                ->addViolation();
        }
    }
}
