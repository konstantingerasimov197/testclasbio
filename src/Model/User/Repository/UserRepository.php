<?php

declare(strict_types=1);

namespace App\Model\User\Repository;

use App\Model\Base\Repository\BaseEntityRepository;
use App\Model\Base\Repository\Filter;
use App\Model\User\Entity\User;
use App\Model\User\Value\UserId;

/**
 * Репозиторий с пользователями
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 *
 * @method User getById(UserId $id)
 * @method User getByFilter(Filter $filter)
 * @method User|null findByFilter(Filter $filter)
 * @method User[] findAll()
 * @method User[] findAllByIds(UserId[] $ids)
 * @method User[] findAllByFilter(Filter $filter)
 * @method User[] findAllByPagination(int $perPage, int $offset)
 * @method User[] searchByFilter(Filter $filter, int $perPage, int $offset)
 * @method bool existsById(UserId $id)
 * @method void persist(User $entity)
 * @method void remove(User $entity)
 */
class UserRepository extends BaseEntityRepository
{
    /**
     * @inheritDoc
     */
    protected function getEntityClass(): string
    {
        return User::class;
    }
}
