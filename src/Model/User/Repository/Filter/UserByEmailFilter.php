<?php

declare(strict_types=1);

namespace App\Model\User\Repository\Filter;

use App\Model\Base\Repository\Filter;
use Doctrine\ORM\QueryBuilder;

/**
 * Фильтр пользователей по
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserByEmailFilter implements Filter
{
    private string $email;

    /**
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * @inheritDoc
     */
    public function apply(QueryBuilder $qb, bool $isAggregate = false): void
    {
        $expr = $qb->expr();

        $qb->andWhere(
            $expr->eq(
                't.email',
                $expr->literal($this->email)
            )
        );
    }
}
