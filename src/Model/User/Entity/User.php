<?php

declare(strict_types=1);

namespace App\Model\User\Entity;

use App\Model\Base\Entity\EntityInterface;
use App\Model\User\Enum\UserRoleEnum;
use App\Model\User\Value\UserId;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * Пользователь
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 *
 * @ORM\Entity(
 *     repositoryClass="App\Model\User\Repository\UserRepository",
 * )
 * @ORM\Table(
 *     name="`user`",
 *     indexes={
 *         @ORM\Index(name="IX_USER_ACTIVE", columns={"active"}),
 *         @ORM\Index(name="IX_USER_ROLES", columns={"roles"}),
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="IX_USER_EMAIL", columns={"email"}),
 *     },
 * )
 */
class User implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="user_id")
     */
    private UserId $id;

    /**
     * @ORM\Column(name="email", type="string")
     */
    private string $email;

    /**
     * @ORM\Column(name="active", type="boolean")
     */
    private bool $active;

    /**
     * @ORM\Column(name="password", type="string")
     */
    private string $password;

    /**
     * @var UserRoleEnum[]
     *
     * @ORM\Column(name="roles", type="user_roles")
     */
    private array $roles;

    /**
     * @ORM\Column(name="created_at", type="datetime_immutable")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime_immutable")
     */
    private DateTimeImmutable $updatedAt;

    /**
     * @param UserId $id
     * @param string $email
     * @param string $password
     * @param UserRoleEnum[] $roles
     *
     * @throws Exception
     */
    public function __construct(UserId $id, string $email, string $password, array $roles)
    {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->roles = $roles;
        $this->active = true;

        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = $this->createdAt;
    }

    /**
     * Обновление пользователя
     *
     * @param string $email
     * @param string|null $password
     * @param UserRoleEnum[] $roles
     * @param bool $isActive
     *
     * @throws Exception
     */
    public function update(string $email, ?string $password, array $roles, bool $isActive): void
    {
        $this->email = $email;
        $this->active = $isActive;
        $this->roles = $roles;
        $this->updatedAt = new DateTimeImmutable();

        if ($password !== null) {
            $this->password = $password;
        }
    }

    /**
     * @inheritDoc
     */
    public function getId(): UserId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return UserRoleEnum[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }
}
