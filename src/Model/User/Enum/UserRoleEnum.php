<?php

declare(strict_types=1);

namespace App\Model\User\Enum;

use App\Model\Base\Entity\Enum;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 *
 * @method static UserRoleEnum get(string $enumerator)
 * @method string getValue()
 */
class UserRoleEnum extends Enum
{
    public const ADMIN = 'ROLE_ADMIN';

    /**
     * @return UserRoleEnum
     */
    public static function admin(): self
    {
        return self::get(self::ADMIN);
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->is(self::ADMIN);
    }
}
