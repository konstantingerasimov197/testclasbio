<?php

declare(strict_types=1);

namespace App\Model\User\Value;

use App\Model\Base\Entity\BaseEntityUuidId;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserId extends BaseEntityUuidId
{
}
