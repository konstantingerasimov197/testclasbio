<?php

declare(strict_types=1);

namespace App\Model\User\Query\Search;

use App\Model\User\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Обработчик поиска пользователей с пагинацией
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserSearchQueryHandler implements MessageHandlerInterface
{
    private UserRepository $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param UserSearchQuery $query
     *
     * @return UserSearchQueryResult
     */
    public function __invoke(UserSearchQuery $query): UserSearchQueryResult
    {
        $count = $this->repository->countAll();

        $list = $this->repository->findAllByPagination(
            $query->getLimit(),
            $query->getOffset()
        );

        return new UserSearchQueryResult($list, $count);
    }
}
