<?php

declare(strict_types=1);

namespace App\Model\User\Query\Search;

use App\Infrastructure\MessageBus\Query\QueryCommandInterface;

/**
 * Поиск пользователей с пагинацией
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserSearchQuery implements QueryCommandInterface
{
    private int $limit;
    private int $offset;

    /**
     * @param int $limit
     * @param int $offset
     */
    public function __construct(int $limit, int $offset)
    {
        $this->limit = $limit;
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }
}
