<?php

declare(strict_types=1);

namespace App\Model\User\Query\Search;

use App\Infrastructure\MessageBus\Query\QueryResultInterface;
use App\Model\User\Entity\User;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserSearchQueryResult implements QueryResultInterface
{
    /**
     * @var User[]
     */
    private array $list;
    private int $count;

    /**
     * @param User[]|array $list
     * @param int $count
     */
    public function __construct($list, int $count)
    {
        $this->list = $list;
        $this->count = $count;
    }

    /**
     * @return User[]
     */
    public function getList(): array
    {
        return $this->list;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }
}
