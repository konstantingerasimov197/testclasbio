<?php

declare(strict_types=1);

namespace App\Model\User\Query\GetById;

use App\Model\User\Repository\UserRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Обработчик получения пользователя по идентификатору
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserByIdQueryHandler implements MessageHandlerInterface
{
    private UserRepository $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param UserByIdQuery $query
     *
     * @return UserByIdQueryResult
     * @throws NoResultException
     */
    public function __invoke(UserByIdQuery $query): UserByIdQueryResult
    {
        $user = $this->repository->getById($query->getUserId());

        return new UserByIdQueryResult($user);
    }
}
