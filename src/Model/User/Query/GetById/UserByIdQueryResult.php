<?php

declare(strict_types=1);

namespace App\Model\User\Query\GetById;

use App\Infrastructure\MessageBus\Query\QueryResultInterface;
use App\Model\User\Entity\User;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserByIdQueryResult implements QueryResultInterface
{
    private User $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
