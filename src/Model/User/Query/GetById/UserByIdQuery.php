<?php

declare(strict_types=1);

namespace App\Model\User\Query\GetById;

use App\Infrastructure\MessageBus\Query\QueryCommandInterface;
use App\Model\User\Value\UserId;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserByIdQuery implements QueryCommandInterface
{
    private UserId $userId;

    /**
     * @param UserId $userId
     */
    public function __construct(UserId $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return UserId
     */
    public function getUserId(): UserId
    {
        return $this->userId;
    }
}
