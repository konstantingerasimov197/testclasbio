<?php

declare(strict_types=1);

namespace App\Model\User\UseCase\Create;

use App\Infrastructure\MessageBus\Command\CommandInterface;
use App\Infrastructure\MessageBus\ValidableMessageInterface;
use App\Model\User\Enum\UserRoleEnum;
use App\Model\User\Validation\UserEmailUnique;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserCreateCommand implements CommandInterface, ValidableMessageInterface
{
    private string $email;
    private string $password;

    /**
     * @var UserRoleEnum[]
     */
    private array $roles;

    /**
     * @param string $email
     * @param string $password
     * @param UserRoleEnum[] $roles
     */
    public function __construct(string $email, string $password, array $roles)
    {
        $this->email = $email;
        $this->password = $password;
        $this->roles = $roles;
    }

    /**
     * @inheritDoc
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata): void
    {
        $metadata
            ->addPropertyConstraints('email', [
                new UserEmailUnique(),
            ]);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return UserRoleEnum[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }
}
