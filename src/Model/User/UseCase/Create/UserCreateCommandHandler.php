<?php

declare(strict_types=1);

namespace App\Model\User\UseCase\Create;

use App\Infrastructure\Doctrine\EntityManager\Flusher;
use App\Model\User\Entity\User;
use App\Model\User\Repository\UserRepository;
use App\Model\User\Value\UserId;
use Exception;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * Обработчик добавления пользователя
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserCreateCommandHandler implements MessageHandlerInterface
{
    private PasswordEncoderInterface $encoder;
    private UserRepository $repository;
    private Flusher $flusher;

    /**
     * @param PasswordEncoderInterface $nativeEncoder
     * @param UserRepository $repository
     * @param Flusher $flusher
     */
    public function __construct(
        PasswordEncoderInterface $nativeEncoder,
        UserRepository $repository,
        Flusher $flusher
    ) {
        $this->encoder = $nativeEncoder;
        $this->repository = $repository;
        $this->flusher = $flusher;
    }

    /**
     * @param UserCreateCommand $command
     *
     * @throws Exception
     */
    public function __invoke(UserCreateCommand $command): void
    {
        $password = $this->encoder->encodePassword($command->getPassword(), null);

        $user = new User(
            UserId::next(),
            $command->getEmail(),
            $password,
            $command->getRoles()
        );

        $this->repository->persist($user);
        $this->flusher->flush();
    }
}
