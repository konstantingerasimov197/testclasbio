<?php

declare(strict_types=1);

namespace App\Model\User\UseCase\Update;

use App\Infrastructure\Doctrine\EntityManager\Flusher;
use App\Model\User\Repository\UserRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * Обработчика обновленяи пользователя
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserUpdateCommandHandler implements MessageHandlerInterface
{
    private PasswordEncoderInterface $encoder;
    private UserRepository $repository;
    private Flusher $flusher;

    /**
     * @param PasswordEncoderInterface $encoder
     * @param UserRepository $repository
     * @param Flusher $flusher
     */
    public function __construct(
        PasswordEncoderInterface $encoder,
        UserRepository $repository,
        Flusher $flusher
    ) {
        $this->encoder = $encoder;
        $this->repository = $repository;
        $this->flusher = $flusher;
    }

    /**
     * @param UserUpdateCommand $command
     *
     * @throws NoResultException
     */
    public function __invoke(UserUpdateCommand $command): void
    {
        $user = $this->repository->getById($command->getUserId());

        $password = $command->getPassword();
        if ($password !== null) {
            $password = $this->encoder->encodePassword($password, null);
        }

        $user->update(
            $command->getEmail(),
            $password,
            $command->getRoles(),
            $command->isActive()
        );

        $this->flusher->flush();
    }
}
