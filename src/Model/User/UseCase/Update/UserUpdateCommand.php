<?php

declare(strict_types=1);

namespace App\Model\User\UseCase\Update;

use App\Infrastructure\MessageBus\Command\CommandInterface;
use App\Infrastructure\MessageBus\ValidableMessageInterface;
use App\Model\User\Enum\UserRoleEnum;
use App\Model\User\Validation\UserEmailUnique;
use App\Model\User\Validation\UserExists;
use App\Model\User\Value\UserId;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserUpdateCommand implements CommandInterface, ValidableMessageInterface
{
    private UserId $userId;
    private string $email;
    private ?string $password;
    private bool $isActive;

    /**
     * @var UserRoleEnum[]
     */
    private array $roles;

    /**
     * @param UserId $userId
     * @param string $email
     * @param string|null $password
     * @param UserRoleEnum[] $roles
     * @param bool $isActive
     */
    public function __construct(UserId $userId, string $email, ?string $password, array $roles, bool $isActive)
    {
        $this->userId = $userId;
        $this->email = $email;
        $this->password = $password;
        $this->roles = $roles;
        $this->isActive = $isActive;
    }

    /**
     * @inheritDoc
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata): void
    {
        $metadata
            ->addPropertyConstraints('userId', [
                new UserExists(),
            ])
            ->addPropertyConstraints('email', [
                new UserEmailUnique([
                    'getIdCallback' => static function (UserUpdateCommand $command): UserId {
                        return $command->getUserId();
                    },
                ]),
            ]);
    }

    /**
     * @return UserId
     */
    public function getUserId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @return UserRoleEnum[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }
}
