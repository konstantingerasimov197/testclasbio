<?php

declare(strict_types=1);

namespace App\Model\User\UseCase\Delete;

use App\Infrastructure\MessageBus\Command\CommandInterface;
use App\Infrastructure\MessageBus\ValidableMessageInterface;
use App\Model\User\Validation\UserExists;
use App\Model\User\Value\UserId;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserDeleteCommand implements CommandInterface, ValidableMessageInterface
{
    private UserId $userId;

    /**
     * @param UserId $userId
     */
    public function __construct(UserId $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @inheritDoc
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata): void
    {
        $metadata->addPropertyConstraints('userId', [
            new UserExists(),
        ]);
    }

    /**
     * @return UserId
     */
    public function getUserId(): UserId
    {
        return $this->userId;
    }
}
