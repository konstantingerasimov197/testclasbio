<?php

declare(strict_types=1);

namespace App\Model\User\UseCase\Delete;

use App\Infrastructure\Doctrine\EntityManager\Flusher;
use App\Model\User\Repository\UserRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Обработчик удаления пользователя
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserDeleteCommandHandler implements MessageHandlerInterface
{
    private UserRepository $repository;
    private Flusher $flusher;

    /**
     * @param UserRepository $repository
     * @param Flusher $flusher
     */
    public function __construct(UserRepository $repository, Flusher $flusher)
    {
        $this->repository = $repository;
        $this->flusher = $flusher;
    }

    /**
     * @param UserDeleteCommand $command
     *
     * @throws NoResultException
     * @throws ORMException
     */
    public function __invoke(UserDeleteCommand $command): void
    {
        $user = $this->repository->getById($command->getUserId());
        $this->repository->remove($user);

        $this->flusher->flush();
    }
}
