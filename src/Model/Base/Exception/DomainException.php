<?php

declare(strict_types=1);

namespace App\Model\Base\Exception;

use Exception;
use Throwable;

/**
 * Базовый класс для ошибок бизнес-логики
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class DomainException extends Exception
{
    private string $errorCode;

    /**
     * @param string $message
     * @param string $errorCode
     * @param Throwable|null $throwable
     */
    public function __construct(string $message, string $errorCode, ?Throwable $throwable = null)
    {
        parent::__construct($message, 0, $throwable);
        $this->errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }
}
