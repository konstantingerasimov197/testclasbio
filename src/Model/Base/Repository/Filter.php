<?php

declare(strict_types=1);

namespace App\Model\Base\Repository;

use Doctrine\ORM\QueryBuilder;

/**
 * Интерфейс для реализации модифицирующих DQL фильтров
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
interface Filter
{
    /**
     * Модифицирует DQL запрос
     *
     * @param QueryBuilder $qb
     * @param bool $isAggregate Флаг, позволяющий определить тип запроса
     *  - true, если запрос агрегирующий (max, count и тд)
     *  - false, если запрос на получение сущностей(-ти)
     */
    public function apply(QueryBuilder $qb, bool $isAggregate = false): void;
}
