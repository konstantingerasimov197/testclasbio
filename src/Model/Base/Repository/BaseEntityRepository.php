<?php

declare(strict_types=1);

namespace App\Model\Base\Repository;

use App\Model\Base\Entity\EntityIdInterface;
use App\Model\Base\Entity\EntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Абстрактный класс с методами поиска с фильтрами для репозиториев
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class BaseEntityRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, $this->getEntityClass());
    }

    /**
     * Получение сущности по идентификатору
     *
     * @param EntityIdInterface $id
     *
     * @return EntityInterface
     * @throws NoResultException
     */
    public function getById(EntityIdInterface $id): EntityInterface
    {
        /** @var EntityInterface|null $entity */
        $entity = $this->find($id);

        if ($entity === null) {
            throw new NoResultException();
        }

        return $entity;
    }

    /**
     * Получение сущностей по их идентификаторам
     *
     * @param EntityIdInterface[] $ids
     *
     * @return EntityInterface[]
     */
    public function findAllByIds(array $ids): array
    {
        return $this->findBy([
            'id' => $ids,
        ]);
    }

    /**
     * Получение сущности с применением фильтра к запросу
     *
     * @param Filter $filter
     *
     * @return EntityInterface
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getByFilter(Filter $filter): EntityInterface
    {
        $query = $this->createQueryBuilder('t');
        $filter->apply($query);
        $entity = $query->getQuery()->getOneOrNullResult();

        if ($entity === null) {
            throw new NoResultException();
        }

        return $entity;
    }

    /**
     * Получение сущности с применением фильтра к запросу
     *
     * @param Filter $filter
     *
     * @return EntityInterface|null
     */
    public function findByFilter(Filter $filter): ?EntityInterface
    {
        try {
            $query = $this->createQueryBuilder('t');
            $filter->apply($query);

            return $query->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $ex) {
            return null;
        }
    }

    /**
     * Возвращает массив сущностей
     *
     * @param Filter $filter
     *
     * @return EntityInterface[]
     */
    public function findAllByFilter(Filter $filter): array
    {
        $query = $this->createQueryBuilder('t');
        $filter->apply($query);

        return $query->getQuery()->getResult();
    }

    /**
     * Возвращает массив сущностей с пагинацией
     *
     * @param int $perPage
     * @param int $offset
     *
     * @return EntityInterface[]
     */
    public function findAllByPagination(int $perPage, int $offset): array
    {
        return $this->findBy([], null, $perPage, $offset);
    }

    /**
     * Возвращает массив сущностей с пагинацией
     *
     * @param Filter $filter
     * @param int $perPage
     * @param int $offset
     *
     * @return EntityInterface[]
     */
    public function searchByFilter(Filter $filter, int $perPage, int $offset): array
    {
        $query = $this->createQueryBuilder('t')
            ->setMaxResults($perPage)
            ->setFirstResult($offset);
        $filter->apply($query);

        return $query->getQuery()->getResult();
    }

    /**
     * Проверка на существование сущности с применением фильтра
     *
     * @param Filter $filter
     *
     * @return bool
     * @throws NonUniqueResultException
     */
    public function existsByFilter(Filter $filter): bool
    {
        try {
            $query = $this->createQueryBuilder('t');
            $filter->apply($query);
            $query->select('1');

            return (bool)$query->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $exception) {
            return false;
        }
    }

    /**
     * Проверка на существование сущности по идентификатору
     *
     * @param EntityIdInterface $id
     *
     * @return bool
     */
    public function existsById(EntityIdInterface $id): bool
    {
        return $this->exists(['id' => $id]);
    }

    /**
     * Получение общего количества сущностей
     *
     * @return int
     */
    public function countAll(): int
    {
        return $this->count([]);
    }

    /**
     * Возвращает количество сущностей
     *
     * @param Filter $filter
     *
     * @return int
     * @throws NonUniqueResultException
     */
    public function countByFilter(Filter $filter): int
    {
        try {
            $expr = $this->getEntityManager()->getExpressionBuilder();
            $query = $this->createQueryBuilder('t')
                ->select($expr->count('1'));

            $filter->apply($query, true);

            return (int)$query->getQuery()->getSingleScalarResult();
        } catch (NoResultException $exception) {
            return 0;
        }
    }

    /**
     * @param EntityInterface $entity
     *
     * @throws ORMException
     */
    public function persist(EntityInterface $entity): void
    {
        $this->getEntityManager()->persist($entity);
    }

    /**
     * @param EntityInterface $entity
     *
     * @throws ORMException
     */
    public function remove(EntityInterface $entity): void
    {
        $this->getEntityManager()->remove($entity);
    }

    /**
     * @param array $criteria
     *
     * @return bool
     */
    protected function exists(array $criteria): bool
    {
        return $this->count($criteria) > 0;
    }

    /**
     * Получение класса сущности
     *
     * @return string
     */
    abstract protected function getEntityClass(): string;
}
