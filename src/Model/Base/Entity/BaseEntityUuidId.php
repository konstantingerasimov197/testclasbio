<?php

declare(strict_types=1);

namespace App\Model\Base\Entity;

use Exception;
use Ramsey\Uuid\Uuid;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class BaseEntityUuidId implements EntityIdInterface
{
    protected string $id;

    /**
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return static
     * @throws Exception
     */
    public static function next(): self
    {
        $uuid = Uuid::uuid4()->toString();
        return new static($uuid);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param static $dest
     *
     * @return bool
     */
    public function equals(self $dest): bool
    {
        return $this->id === $dest->id;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->id;
    }
}
