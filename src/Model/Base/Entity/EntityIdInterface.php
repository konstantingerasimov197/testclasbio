<?php

declare(strict_types=1);

namespace App\Model\Base\Entity;

/**
 * Интерфейс для всех ID сущностей доктрины
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
interface EntityIdInterface
{
    /**
     * @return int|string
     */
    public function getId();
}
