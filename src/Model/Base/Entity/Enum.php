<?php

declare(strict_types=1);

namespace App\Model\Base\Entity;

use MabeEnum\Enum as BaseEnum;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class Enum extends BaseEnum
{
    /**
     * @return static
     */
    public static function getRandom(): self
    {
        $values = self::getEnumerators();
        return $values[array_rand($values)];
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        /** @var bool|float|int|string|null $value */
        $value = $this->getValue();
        return (string)$value;
    }
}
