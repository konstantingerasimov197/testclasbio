<?php

declare(strict_types=1);

namespace App\Model\Base\Entity;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class BaseEntityIntId implements EntityIdInterface
{
    protected int $id;

    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param static $dest
     *
     * @return bool
     */
    public function equals(self $dest): bool
    {
        return $this->id === $dest->id;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->id;
    }
}
