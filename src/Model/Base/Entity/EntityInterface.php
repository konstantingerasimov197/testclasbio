<?php

declare(strict_types=1);

namespace App\Model\Base\Entity;

/**
 * Интерфейс для всех сущностей доктрины
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
interface EntityInterface
{
    /**
     * @return EntityIdInterface
     */
    public function getId(): EntityIdInterface;
}
