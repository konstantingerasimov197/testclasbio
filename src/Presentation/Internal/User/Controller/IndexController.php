<?php

declare(strict_types=1);

namespace App\Presentation\Internal\User\Controller;

use App\Model\User\Query\Search\UserSearchQuery;
use App\Model\User\Query\Search\UserSearchQueryResult;
use App\Presentation\Base\Controller\BaseController;
use App\Presentation\Base\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Контроллер списка пользователей
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class IndexController extends BaseController
{
    /**
     * @Route(
     *     methods={"GET"},
     *     path="/users",
     *     name="user_index"
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function run(Request $request): Response
    {
        $pagination = $this->getPaginationParameters($request);

        $query = new UserSearchQuery(
            $pagination->getPerPage(),
            $pagination->getOffset(),
        );

        /** @var UserSearchQueryResult $result */
        $result = $this->handleQuery($query);

        return $this->render('internal/user/index.html.twig', [
            'list' => $result->getList(),
            'paginator' => new Paginator(
                $pagination,
                $result->getCount()
            ),
        ]);
    }
}
