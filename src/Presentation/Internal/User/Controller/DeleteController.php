<?php

declare(strict_types=1);

namespace App\Presentation\Internal\User\Controller;

use App\Model\User\UseCase\Delete\UserDeleteCommand;
use App\Model\User\Value\UserId;
use App\Presentation\Base\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Контроллер удаления пользователя
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class DeleteController extends BaseController
{
    /**
     * @Route(
     *     methods={"POST"},
     *     path="/users/delete/{userId}",
     *     name="user_delete",
     *
     *     requirements={
     *         "userId"="^[\w\-]+$"
     *     }
     * )
     *
     * @param UserId $userId
     *
     * @return Response
     */
    public function run(UserId $userId): Response
    {
        $command = new UserDeleteCommand($userId);
        $this->handleCommand($command);

        return $this->redirectToRoute('user_index');
    }
}
