<?php

declare(strict_types=1);

namespace App\Presentation\Internal\User\Controller;

use App\Model\User\Query\GetById\UserByIdQuery;
use App\Model\User\Query\GetById\UserByIdQueryResult;
use App\Model\User\UseCase\Update\UserUpdateCommand;
use App\Model\User\Value\UserId;
use App\Presentation\Base\Controller\BaseController;
use App\Presentation\Internal\User\Data\UpdateFormData;
use App\Presentation\Internal\User\Exception\UserNotFoundException;
use App\Presentation\Internal\User\Form\UpdateForm;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Контроллер обновления пользователя
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UpdateController extends BaseController
{
    /**
     * @Route(
     *     methods={"GET", "POST"},
     *     path="/users/update/{userId}",
     *     name="user_update",
     *
     *     requirements={
     *         "userId"="^[\w\-]+$"
     *     }
     * )
     *
     * @param Request $request
     * @param UserId $userId
     *
     * @return Response
     */
    public function run(Request $request, UserId $userId): Response
    {
        $data = $this->getData($userId);
        $form = $this->createForm(UpdateForm::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->handle($userId, $data);
                return $this->redirectToRoute('user_index');
            } catch (ValidationFailedException $exception) {
                $this->addErrorsToForm($form, $exception->getViolations());
            }
        }

        return $this->render('internal/user/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param UserId $userId
     *
     * @return UpdateFormData
     */
    private function getData(UserId $userId): UpdateFormData
    {
        try {
            $query = new UserByIdQuery($userId);
            /** @var UserByIdQueryResult $result */
            $result = $this->handleQuery($query);
        } catch (NoResultException $exception) {
            throw new UserNotFoundException();
        }

        return new UpdateFormData($result->getUser());
    }

    /**
     * @param UserId $userId
     * @param UpdateFormData $data
     */
    private function handle(UserId $userId, UpdateFormData $data): void
    {
        $command = new UserUpdateCommand(
            $userId,
            $data->email,
            $data->password,
            [$data->role],
            $data->isActive
        );

        $this->handleCommand($command);
    }
}
