<?php

declare(strict_types=1);

namespace App\Presentation\Internal\User\Controller;

use App\Model\User\UseCase\Create\UserCreateCommand;
use App\Presentation\Base\Controller\BaseController;
use App\Presentation\Internal\User\Data\CreateFormData;
use App\Presentation\Internal\User\Form\CreateForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class CreateController extends BaseController
{
    /**
     * @Route(
     *     methods={"GET", "POST"},
     *     path="/users/create",
     *     name="user_create"
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function run(Request $request): Response
    {
        $data = new CreateFormData();
        $form = $this->createForm(CreateForm::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->handle($data);
                return $this->redirectToRoute('user_index');
            } catch (ValidationFailedException $exception) {
                $this->addErrorsToForm($form, $exception->getViolations());
            }
        }

        return $this->render('internal/user/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param CreateFormData $data
     */
    private function handle(CreateFormData $data): void
    {
        $command = new UserCreateCommand(
            $data->email,
            $data->password,
            [$data->role]
        );

        $this->handleCommand($command);
    }
}
