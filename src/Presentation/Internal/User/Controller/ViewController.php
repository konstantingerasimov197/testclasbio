<?php

declare(strict_types=1);

namespace App\Presentation\Internal\User\Controller;

use App\Model\User\Query\GetById\UserByIdQuery;
use App\Model\User\Query\GetById\UserByIdQueryResult;
use App\Model\User\Value\UserId;
use App\Presentation\Base\Controller\BaseController;
use App\Presentation\Internal\User\Exception\UserNotFoundException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Контроллер просмотра пользователя
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class ViewController extends BaseController
{
    /**
     * @Route(
     *     methods={"GET"},
     *     path="/users/view/{userId}",
     *     name="user_view",
     *
     *     requirements={
     *         "userId"="^[\w\-]+$"
     *     }
     * )
     *
     * @param UserId $userId
     *
     * @return Response
     */
    public function run(UserId $userId): Response
    {
        try {
            $query = new UserByIdQuery($userId);

            /** @var UserByIdQueryResult $result */
            $result = $this->handleQuery($query);
        } catch (NoResultException $exception) {
            throw new UserNotFoundException();
        }

        return $this->render('internal/user/view.html.twig', [
            'user' => $result->getUser(),
        ]);
    }
}
