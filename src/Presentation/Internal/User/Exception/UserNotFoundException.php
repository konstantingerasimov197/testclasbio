<?php

declare(strict_types=1);

namespace App\Presentation\Internal\User\Exception;

use App\Presentation\Base\Exception\NotFoundHttpException;

/**
 * Пользователь не найден
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserNotFoundException extends NotFoundHttpException
{
    private const CODE = '086b71a5-b2dc-4976-aa95-a049b99ef4be';

    public function __construct()
    {
        parent::__construct(
            'Пользователь не найден',
            self::CODE
        );
    }
}
