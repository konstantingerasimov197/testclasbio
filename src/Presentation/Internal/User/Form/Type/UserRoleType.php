<?php

declare(strict_types=1);

namespace App\Presentation\Internal\User\Form\Type;

use App\Model\User\Enum\UserRoleEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserRoleType extends AbstractType implements DataTransformerInterface
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer($this);
    }

    /**
     * @inheritDoc
     */
    public function transform($data)
    {
        if ($data instanceof UserRoleEnum) {
            return $data->getValue();
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function reverseTransform($data)
    {
        if (!is_string($data) || empty($data)) {
            return null;
        }

        return UserRoleEnum::get($data);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choices' => [
                'Администратор' => UserRoleEnum::ADMIN,
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
