<?php

declare(strict_types=1);

namespace App\Presentation\Internal\User\Form;

use App\Presentation\Base\Form\BaseForm;
use App\Presentation\Internal\User\Form\Type\UserRoleType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Форма для обновления пользователя
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UpdateForm extends BaseForm
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Email(),
                ],
            ])
            ->add('password', PasswordType::class, [
                'required' => false,
                'constraints' => [
                    new Length([
                        'allowEmptyString' => false,
                        'min' => 6,
                    ]),
                ],
            ])
            ->add('role', UserRoleType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('isActive', CheckboxType::class)
            ->add('submit', SubmitType::class);
    }
}
