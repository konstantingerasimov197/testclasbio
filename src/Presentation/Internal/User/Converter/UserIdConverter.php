<?php

declare(strict_types=1);

namespace App\Presentation\Internal\User\Converter;

use App\Model\User\Value\UserId;
use App\Presentation\Base\Converter\BaseUuidConverter;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserIdConverter extends BaseUuidConverter
{
    /**
     * @inheritDoc
     */
    protected function getEntityIdClass(): string
    {
        return UserId::class;
    }
}
