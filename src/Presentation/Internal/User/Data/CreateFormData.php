<?php

declare(strict_types=1);

namespace App\Presentation\Internal\User\Data;

use App\Model\User\Enum\UserRoleEnum;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class CreateFormData
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var UserRoleEnum
     */
    public $role;
}
