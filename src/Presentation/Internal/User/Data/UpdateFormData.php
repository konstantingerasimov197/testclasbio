<?php

declare(strict_types=1);

namespace App\Presentation\Internal\User\Data;

use App\Model\User\Entity\User;
use App\Model\User\Enum\UserRoleEnum;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UpdateFormData
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var UserRoleEnum
     */
    public $role;

    /**
     * @var bool
     */
    public $isActive;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->email = $user->getEmail();
        $this->isActive = $user->isActive();
        $this->role = $user->getRoles()[0];
    }
}
