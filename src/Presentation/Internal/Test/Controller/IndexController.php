<?php

declare(strict_types=1);

namespace App\Presentation\Internal\Test\Controller;

use App\Presentation\Base\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Тестовый контроллер для демонстрации
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class IndexController extends BaseController
{
    /**
     * @Route(
     *     methods={"GET"},
     *     path="/",
     *     name="home"
     * )
     *
     * @return Response
     */
    public function run(): Response
    {
        return $this->render('internal/test/index.html.twig', [
            'class' => self::class,
        ]);
    }
}
