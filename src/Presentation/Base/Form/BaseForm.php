<?php

declare(strict_types=1);

namespace App\Presentation\Base\Form;

use Symfony\Component\Form\AbstractType;

/**
 * Абстрактная форма
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class BaseForm extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
