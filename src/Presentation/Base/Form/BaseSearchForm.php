<?php

declare(strict_types=1);

namespace App\Presentation\Base\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Абстрактная форма для поиска
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class BaseSearchForm extends BaseForm
{
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
        ]);
    }
}
