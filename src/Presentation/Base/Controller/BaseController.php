<?php

declare(strict_types=1);

namespace App\Presentation\Base\Controller;

use App\Infrastructure\MessageBus\Command\CommandBusAwareInterface;
use App\Infrastructure\MessageBus\Command\CommandBusAwareTrait;
use App\Infrastructure\MessageBus\Query\QueryBusAwareInterface;
use App\Infrastructure\MessageBus\Query\QueryBusAwareTrait;
use App\Presentation\Base\Pagination\PaginationParameters;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Базовый контроллер
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class BaseController extends AbstractController implements
    CommandBusAwareInterface,
    QueryBusAwareInterface
{
    use CommandBusAwareTrait;
    use QueryBusAwareTrait;

    /**
     * Получение входящих параметров для пагинации
     *
     * @param Request $request
     *
     * @return PaginationParameters
     */
    protected function getPaginationParameters(Request $request): PaginationParameters
    {
        return new PaginationParameters($request);
    }

    /**
     * Обработка формы
     *
     * @param FormInterface $form
     * @param ConstraintViolationListInterface $violations
     */
    protected function addErrorsToForm(FormInterface $form, ConstraintViolationListInterface $violations): void
    {
        /** @var ConstraintViolation $violation */
        foreach ($violations as $violation) {
            /** @var string $message */
            $message = $violation->getMessage();

            $form->addError(new FormError(
                $message,
                $violation->getMessageTemplate(),
                $violation->getParameters()
            ));
        }
    }
}
