<?php

declare(strict_types=1);

namespace App\Presentation\Base\Exception;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException as BaseException;
use Throwable;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class BadRequestHttpException extends BaseException
{
    private string $errorCode;

    /**
     * @param string $message
     * @param string $errorCode
     * @param Throwable|null $throwable
     */
    public function __construct(string $message, string $errorCode, ?Throwable $throwable = null)
    {
        parent::__construct($message, $throwable, 400);
        $this->errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }
}
