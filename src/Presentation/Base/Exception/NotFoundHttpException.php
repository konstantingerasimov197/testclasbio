<?php

declare(strict_types=1);

namespace App\Presentation\Base\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as BaseException;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class NotFoundHttpException extends BaseException
{
    private string $errorCode;

    /**
     * @param string $message
     * @param string $errorCode
     */
    public function __construct(string $message, string $errorCode)
    {
        parent::__construct($message, null, 404);
        $this->errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }
}
