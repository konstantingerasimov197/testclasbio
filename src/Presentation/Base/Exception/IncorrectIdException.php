<?php

declare(strict_types=1);

namespace App\Presentation\Base\Exception;

/**
 * Передан некорректный идентификатор
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class IncorrectIdException extends BadRequestHttpException
{
    private const CODE = '14026126-7225-49ea-b724-87bd6de7fcdb';

    public function __construct()
    {
        parent::__construct(
            'Передан некорректный идентификатор',
            self::CODE
        );
    }
}
