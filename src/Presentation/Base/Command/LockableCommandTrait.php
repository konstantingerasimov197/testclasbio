<?php

declare(strict_types=1);

namespace App\Presentation\Base\Command;

use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

/**
 * Трейт для консольных команд, позволяющий оборачивать выполнение в автоматический лок-релиз
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
trait LockableCommandTrait
{
    use LockableTrait;

    private ?string $lockName = null;

    /**
     * @inheritDoc
     *
     * @throws Throwable
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        if (!$this->lock($this->lockName)) {
            $this->info(
                sprintf('Команда %s уже запущена', $this->getName()),
                [
                    'func_name' => __METHOD__,
                ]
            );

            return 0;
        }

        try {
            return parent::execute($input, $output);
        } catch (Throwable $exception) {
            throw $exception;
        } finally {
            $this->release();
        }
    }

    /**
     * @param string|null $lockName
     *
     * @return static
     */
    public function setLockName(?string $lockName): self
    {
        $this->lockName = $lockName;

        return $this;
    }
}
