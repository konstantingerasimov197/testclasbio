<?php

declare(strict_types=1);

namespace App\Presentation\Base\Command;

use App\Infrastructure\MessageBus\Command\CommandBusAwareInterface;
use App\Infrastructure\MessageBus\Command\CommandBusAwareTrait;
use App\Infrastructure\MessageBus\Query\QueryBusAwareInterface;
use App\Infrastructure\MessageBus\Query\QueryBusAwareTrait;
use Exception;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Throwable;

/**
 * Базовый класс для простых консольных команд
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class BaseCommand extends Command implements
    CommandBusAwareInterface,
    QueryBusAwareInterface,
    LoggerAwareInterface
{
    use CommandBusAwareTrait;
    use QueryBusAwareTrait;
    use LoggerAwareTrait;

    private ?string $startMessage = null;
    private ?string $endMessage = null;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @inheritDoc
     *
     * @throws Throwable
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName() ?: self::class);

        if ($this->startMessage !== null) {
            $this->info($this->startMessage, [
                'func_name' => __METHOD__,
            ]);
        }

        $this->process($input, $output);

        $event = $stopwatch->stop($this->getName() ?: self::class);

        if ($this->endMessage !== null) {
            $this->info(
                sprintf(
                    '%s time: %s seconds, memory: %s Mb',
                    $this->endMessage,
                    $event->getDuration() / 1000,
                    $event->getMemory() / 1048576
                ),
                [
                    'func_name' => __METHOD__,
                ]
            );
        }

        return 0;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws Exception
     */
    abstract protected function process(InputInterface $input, OutputInterface $output): void;

    /**
     * @param string $message
     *
     * @return BaseCommand
     */
    protected function setStartMessage(string $message): self
    {
        $this->startMessage = $message;

        return $this;
    }

    /**
     * @param string $message
     *
     * @return BaseCommand
     */
    protected function setEndMessage(string $message): self
    {
        $this->endMessage = $message;

        return $this;
    }

    /**
     * @param string $message
     * @param array $loggerContext
     */
    protected function info(string $message, array $loggerContext): void
    {
        $this->logger->info($message, array_merge([
            'command_name' => $this->getName(),
        ], $loggerContext));
    }

    /**
     * @param string $message
     * @param array $loggerContext
     */
    protected function debug(string $message, array $loggerContext): void
    {
        $this->logger->debug($message, array_merge([
            'command_name' => $this->getName(),
        ], $loggerContext));
    }

    /**
     * @param string $message
     * @param array $loggerContext
     */
    protected function error(string $message, array $loggerContext): void
    {
        $this->logger->error($message, array_merge([
            'command_name' => $this->getName(),
        ], $loggerContext));
    }
}
