<?php

declare(strict_types=1);

namespace App\Presentation\Base\Converter;

use App\Model\Base\Entity\EntityIdInterface;
use App\Presentation\Base\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Базовый класс для конвертеров строковых идентификаторов
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class BaseUuidConverter implements ParamConverterInterface
{
    /**
     * @inheritDoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();
        if (!$request->attributes->has($param)) {
            return false;
        }

        $value = $request->attributes->get($param);
        if (!is_string($value)) {
            return false;
        }

        $id = $this->getId($value);
        $request->attributes->set($param, $id);

        return true;
    }

    /**
     * Получение идентификатора
     *
     * @param string $value
     *
     * @return EntityIdInterface
     * @throws NotFoundHttpException
     */
    protected function getId(string $value): EntityIdInterface
    {
        $class = $this->getEntityIdClass();
        return new $class($value);
    }

    /**
     * @inheritDoc
     */
    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() === $this->getEntityIdClass();
    }

    /**
     * @return string
     */
    abstract protected function getEntityIdClass(): string;
}
