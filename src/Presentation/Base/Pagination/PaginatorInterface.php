<?php

declare(strict_types=1);

namespace App\Presentation\Base\Pagination;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
interface PaginatorInterface
{
    /**
     * Текущая страница
     *
     * @return int
     */
    public function getCurrentPage(): int;

    /**
     * Всего страниц
     *
     * @return int
     */
    public function getTotalPages(): int;

    /**
     * Всего элементов
     *
     * @return int
     */
    public function getTotal(): int;

    /**
     * Элементов на текущей странице
     *
     * @return int
     */
    public function getCount(): int;

    /**
     * Элементов на страницу
     *
     * @return int
     */
    public function getPerPage(): int;
}
