<?php

declare(strict_types=1);

namespace App\Presentation\Base\Pagination;

/**
 * Пагинатор для списков
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class Paginator implements PaginatorInterface
{
    private PaginationParameters $paginationParams;
    private int $total;
    private int $totalPages;

    /**
     * @param PaginationParameters $paginationParams
     * @param int $total
     */
    public function __construct(PaginationParameters $paginationParams, int $total)
    {
        $this->paginationParams = $paginationParams;
        $this->total = $total;
        $this->totalPages = (int)ceil($this->total / $this->getPerPage());
    }

    /**
     * @inheritDoc
     */
    public function getCurrentPage(): int
    {
        return min(
            $this->paginationParams->getPage(),
            $this->getTotalPages()
        );
    }

    /**
     * @inheritDoc
     */
    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * @inheritDoc
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @inheritDoc
     */
    public function getCount(): int
    {
        if ($this->totalPages !== $this->getCurrentPage()) {
            return $this->getPerPage();
        }

        return $this->total % $this->getPerPage();
    }

    /**
     * @inheritDoc
     */
    public function getPerPage(): int
    {
        return $this->paginationParams->getPerPage();
    }
}
