<?php

declare(strict_types=1);

namespace App\Presentation\Base\Pagination;

use Symfony\Component\HttpFoundation\Request;

/**
 * Параметры для пагинации
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class PaginationParameters
{
    private const PARAM_PER_PAGE = 'per-page';
    private const PARAM_PAGE = 'page';
    private const DEFAULT_PER_PAGE = 20;
    private const MAX_PER_PAGE = 100;
    private const MIN_PAGE = 1;

    private int $perPage;
    private int $page;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $params = $request->query;

        $this->page = max(
            $params->getInt(self::PARAM_PAGE, self::MIN_PAGE),
            self::MIN_PAGE
        );

        $this->perPage = min(
            $params->getInt(self::PARAM_PER_PAGE, self::DEFAULT_PER_PAGE),
            self::MAX_PER_PAGE
        );
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return ($this->page - 1) * $this->perPage;
    }
}
