<?php

declare(strict_types=1);

namespace App\Fixture;

use App\Model\User\Enum\UserRoleEnum;
use App\Tests\Helper\Generator\User\EmailModifierStrategy;
use App\Tests\Helper\Generator\User\PasswordModifierStrategy;
use App\Tests\Helper\Generator\User\RolesModifierStrategy;
use App\Tests\Helper\Generator\User\UserGeneratorStrategy;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserFixture extends BaseFixture
{
    // real password 123456
    private const DEFAULT_PASSWORD = '$argon2i$v=19$m=1024,t=2,p=2$dktLZXhGenFENWlVTVlBQQ$B8HW9iYlFgHr2qz4w/LzrYKt4JlUNQsq9t0fVOhJKQ0';

    /**
     * @inheritDoc
     * @throws Exception
     */
    protected function internalLoad(EntityManagerInterface $manager): void
    {
        $this->addEntity(
            $manager,
            new UserGeneratorStrategy(),
            new EmailModifierStrategy('admin@clasbio.ru'),
            new PasswordModifierStrategy(self::DEFAULT_PASSWORD),
            new RolesModifierStrategy([
                UserRoleEnum::admin(),
            ])
        );
    }
}
