<?php

declare(strict_types=1);

namespace App\Fixture;

use App\Tests\Helper\Generator\GeneratorStrategyInterface;
use App\Tests\Helper\Generator\ModifierStrategyInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;

/**
 * Базовый класс для фикстур
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class BaseFixture extends Fixture
{
    private FakerGenerator $faker;

    public function __construct()
    {
        $this->faker = FakerFactory::create('ru_RU');
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager): void
    {
        /** @var EntityManagerInterface $manager */
        $this->internalLoad($manager);

        $manager->flush();
    }

    /**
     * @param EntityManagerInterface $manager
     */
    abstract protected function internalLoad(EntityManagerInterface $manager): void;

    /**
     * @param EntityManagerInterface $manager
     * @param GeneratorStrategyInterface $generatorStrategy
     * @param ModifierStrategyInterface ...$modifierStrategies
     */
    protected function addEntity(
        EntityManagerInterface $manager,
        GeneratorStrategyInterface $generatorStrategy,
        ModifierStrategyInterface ...$modifierStrategies
    ): void {
        $entity = $generatorStrategy->generate($this->faker);

        foreach ($modifierStrategies as $strategy) {
            $strategy->apply($entity, $this->faker);
        }

        $manager->persist($entity);
    }
}
