## Вводная

#### Стек
- фреймворк Symfony 5
- реляционная бд `PostgreSQL` с помощью [DoctrineBundle](https://symfony.com/doc/current/reference/configuration/doctrine.html)

#### Запуск

- Установить `docker` и `docker-compose`
- Стянуть репозиторий к себе локально
- Перейти в папку проекта
- Запустить в консоли `make init`

## В комплекте
- Сайт - http://127.0.0.1:8070

#### Линтеры:
- `make phplint` - проверяет синтаксические ошибки
- `make phpcs` - проверяет соответствие кода стандартам PSR-12 и PEAR
- `make phpstan` - статический анализатор кода, находит проблемы
- `make tests` - запуск тестов
