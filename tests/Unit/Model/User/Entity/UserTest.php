<?php

declare(strict_types=1);

namespace App\Tests\Unit\Model\User\Entity;

use App\Model\User\Entity\User;
use App\Model\User\Enum\UserRoleEnum;
use App\Model\User\Value\UserId;
use App\Tests\Unit\UnitTestCase;
use Exception;

/**
 * @testdox Тесты агрегата User
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserTest extends UnitTestCase
{
    /**
     * @testdox Обновление пользователя
     *
     * @throws Exception
     */
    public function testUpdate(): void
    {
        $user = new User(
            UserId::next(),
            'test@test.ru',
            'some-password',
            [
                UserRoleEnum::admin(),
            ]
        );

        $user->update(
            'new-email@test.ru',
            'new-password',
            [
                UserRoleEnum::admin(),
            ],
            false
        );

        $this->assertEquals('new-email@test.ru', $user->getEmail());
        $this->assertEquals('new-password', $user->getPassword());
        $this->assertFalse($user->isActive());

        $roles = $user->getRoles();
        $this->assertCount(1, $roles);

        $role = $roles[0];
        $this->assertEnum(UserRoleEnum::admin(), $role);
    }
}
