<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Tests\Assert\PropertyAssert;
use PHPUnit\Framework\TestCase;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class UnitTestCase extends TestCase
{
    use PropertyAssert;
}
