<?php

declare(strict_types=1);

namespace App\Tests\Helper\Fixture;

use App\Model\Base\Entity\EntityIdInterface;
use App\Model\Base\Entity\EntityInterface;
use App\Tests\Helper\Generator\FixtureGeneratorAwareInterface;
use App\Tests\Helper\Generator\FixtureGeneratorAwareTrait;
use App\Tests\Helper\Generator\GeneratorStrategyInterface;
use App\Tests\Helper\Generator\ModifierStrategyInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectManager;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class BaseFixture extends AbstractFixture implements FixtureGeneratorAwareInterface
{
    use FixtureGeneratorAwareTrait;

    /**
     * @inheritDoc
     *
     * @param EntityManagerInterface $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->internalLoad($manager);

        foreach ($this->entities as $entity) {
            $manager->persist($entity);
        }

        $manager->flush();
    }

    /**
     * Внутренняя генерация данных
     *
     * @param EntityManagerInterface $manager
     */
    abstract protected function internalLoad(EntityManagerInterface $manager): void;

    /**
     * @param GeneratorStrategyInterface $generatorStrategy
     * @param ModifierStrategyInterface ...$modifierStrategies
     */
    protected function addEntity(
        GeneratorStrategyInterface $generatorStrategy,
        ModifierStrategyInterface ...$modifierStrategies
    ): void {
        $entity = $this->generator->generate($generatorStrategy, $modifierStrategies);
        $this->entities[] = $entity;
    }

    /**
     * @param string $entityClass
     *
     * @return EntityInterface[]
     */
    protected function getEntities(string $entityClass): array
    {
        return $this->generator->getEntities($entityClass);
    }

    /**
     * @param EntityManagerInterface $manager
     * @param string $entityClass
     * @param int $index
     *
     * @return EntityInterface|object
     * @throws ORMException
     */
    protected function getEntity(EntityManagerInterface $manager, string $entityClass, int $index)
    {
        $id = $this->generator->getEntityId($entityClass, $index);
        return $manager->getReference($entityClass, $id);
    }

    /**
     * @param $entityClass
     * @param int $index
     *
     * @return EntityIdInterface
     */
    protected function getEntityId($entityClass, int $index): EntityIdInterface
    {
        return $this->generator->getEntityId($entityClass, $index);
    }
}
