<?php

namespace App\Tests\Helper;

use ReflectionClass;
use ReflectionException;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class ReflectionHelper
{
    /**
     * @param object $object
     * @param string $property
     * @param $value
     *
     * @throws ReflectionException
     */
    public static function setPrivatePropertyValue($object, string $property, $value): void
    {
        $reflection = new ReflectionClass($object);
        $property = $reflection->getProperty($property);
        $property->setAccessible(true);
        $property->setValue($object, $value);
        $property->setAccessible(false);
    }

    /**
     * @param object $object
     * @param string $property
     *
     * @return mixed
     * @throws ReflectionException
     */
    public static function getPrivatePropertyValue($object, string $property)
    {
        $reflection = new ReflectionClass($object);
        $property = $reflection->getProperty($property);
        $property->setAccessible(true);
        $value = $property->getValue($object);
        $property->setAccessible(false);

        return $value;
    }

    /**
     * @param string $class
     * @param array ...$args
     *
     * @return mixed
     * @throws ReflectionException
     */
    public static function createFromConstructor(string $class, ...$args)
    {
        $reflection = new ReflectionClass($class);
        $constructor = $reflection->getConstructor();
        $constructor->setAccessible(true);

        $object = $reflection->newInstanceWithoutConstructor();
        foreach ($args as $arg) {
            $constructor->invoke($object, $arg);
        }

        $constructor->setAccessible(false);
        return $object;
    }

    /**
     * @param mixed $object
     * @param string $method
     * @param mixed ...$args
     *
     * @return mixed
     * @throws ReflectionException
     */
    public static function callPrivateMethod($object, string $method, ...$args)
    {
        $class = get_class($object);

        $serviceRef = new ReflectionClass($class);
        $methodRef = $serviceRef->getMethod($method);
        $methodRef->setAccessible(true);

        return $methodRef->invokeArgs($object, $args);
    }
}
