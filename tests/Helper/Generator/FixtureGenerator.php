<?php

declare(strict_types=1);

namespace App\Tests\Helper\Generator;

use App\Model\Base\Entity\EntityIdInterface;
use App\Model\Base\Entity\EntityInterface;
use Exception;
use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;

/**
 * Реестр генераторов
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class FixtureGenerator implements FixtureGeneratorInterface
{
    /**
     * @var EntityInterface[][]
     */
    private array $entities = [];
    private FakerGenerator $faker;

    public function __construct()
    {
        $this->faker = FakerFactory::create('ru_RU');
    }

    /**
     * @inheritDoc
     */
    public function generate(
        GeneratorStrategyInterface $generatorStrategy,
        array $modifierStrategies
    ): EntityInterface {
        $entity = $generatorStrategy->generate($this->faker);
        $entityClass = get_class($entity);

        foreach ($modifierStrategies as $modifier) {
            $modifier->apply($entity, $this->faker);
        }

        if (!isset($this->entities[$entityClass])) {
            $this->entities[$entityClass] = [];
        }

        $this->entities[$entityClass][] = $entity;
        return $entity;
    }

    /**
     * @inheritDoc
     */
    public function clear(): void
    {
        $this->entities = [];
        $this->faker->unique(true);
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function getEntities(string $entityClass): array
    {
        if (!isset($this->entities[$entityClass])) {
            throw new Exception(sprintf(
                'Entity of class "%s" is not defined',
                $entityClass
            ));
        }

        return $this->entities[$entityClass];
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function getEntity(string $entityClass, int $index): EntityInterface
    {
        $entities = $this->getEntities($entityClass);

        if (!isset($entities[$index])) {
            throw new Exception(sprintf(
                'Entity of class "%s" with index %d is not defined',
                $entityClass,
                $index
            ));
        }

        return $entities[$index];
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function getEntityId(string $entityClass, int $index): EntityIdInterface
    {
        $entity = $this->getEntity($entityClass, $index);
        return $entity->getId();
    }
}
