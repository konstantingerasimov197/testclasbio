<?php

declare(strict_types=1);

namespace App\Tests\Helper\Generator;

use App\Model\Base\Entity\EntityInterface;
use Faker\Generator as FakerGenerator;

/**
 * Интерфейс для модифицирующий сущность стратегий
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
interface ModifierStrategyInterface
{
    /**
     * Применение стратегии
     *
     * @param EntityInterface $entity
     * @param FakerGenerator $faker
     */
    public function apply(EntityInterface $entity, FakerGenerator $faker): void;
}
