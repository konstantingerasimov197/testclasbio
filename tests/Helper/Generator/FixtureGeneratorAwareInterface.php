<?php

declare(strict_types=1);

namespace App\Tests\Helper\Generator;

use App\Tests\Helper\Generator\FixtureGeneratorInterface;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
interface FixtureGeneratorAwareInterface
{
    /**
     * @param FixtureGeneratorInterface $generator
     */
    public function setFixtureGenerator(FixtureGeneratorInterface $generator): void;
}
