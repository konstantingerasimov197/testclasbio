<?php

declare(strict_types=1);

namespace App\Tests\Helper\Generator\User;

use App\Model\Base\Entity\EntityInterface;
use App\Tests\Helper\Generator\ModifierStrategyInterface;
use App\Tests\Helper\ReflectionHelper;
use Faker\Generator as FakerGenerator;
use ReflectionException;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class PasswordModifierStrategy implements ModifierStrategyInterface
{
    private string $password;

    /**
     * @param string $password
     */
    public function __construct(string $password)
    {
        $this->password = $password;
    }

    /**
     * @inheritDoc
     * @throws ReflectionException
     */
    public function apply(EntityInterface $entity, FakerGenerator $faker): void
    {
        ReflectionHelper::setPrivatePropertyValue(
            $entity,
            'password',
            $this->password
        );
    }
}
