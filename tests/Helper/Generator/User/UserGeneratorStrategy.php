<?php

declare(strict_types=1);

namespace App\Tests\Helper\Generator\User;

use App\Model\Base\Entity\EntityInterface;
use App\Model\User\Entity\User;
use App\Model\User\Enum\UserRoleEnum;
use App\Model\User\Value\UserId;
use App\Tests\Helper\Generator\GeneratorStrategyInterface;
use Exception;
use Faker\Generator as FakerGenerator;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserGeneratorStrategy implements GeneratorStrategyInterface
{
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function generate(FakerGenerator $faker): EntityInterface
    {
        return new User(
            UserId::next(),
            $faker->unique()->email,
            '$argon2i$v=19$m=1024,t=2,p=2$dktLZXhGenFENWlVTVlBQQ$B8HW9iYlFgHr2qz4w/LzrYKt4JlUNQsq9t0fVOhJKQ0',
            [
                UserRoleEnum::admin(),
            ]
        );
    }
}
