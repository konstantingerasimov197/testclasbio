<?php

declare(strict_types=1);

namespace App\Tests\Helper\Generator\User;

use App\Model\Base\Entity\EntityInterface;
use App\Tests\Helper\Generator\ModifierStrategyInterface;
use App\Tests\Helper\ReflectionHelper;
use Faker\Generator as FakerGenerator;
use ReflectionException;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class EmailModifierStrategy implements ModifierStrategyInterface
{
    private string $email;

    /**
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * @inheritDoc
     * @throws ReflectionException
     */
    public function apply(EntityInterface $entity, FakerGenerator $faker): void
    {
        ReflectionHelper::setPrivatePropertyValue(
            $entity,
            'email',
            $this->email
        );
    }
}
