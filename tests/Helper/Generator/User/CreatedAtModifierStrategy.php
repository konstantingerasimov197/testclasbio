<?php

declare(strict_types=1);

namespace App\Tests\Helper\Generator\User;

use App\Model\Base\Entity\EntityInterface;
use App\Tests\Helper\Generator\ModifierStrategyInterface;
use App\Tests\Helper\ReflectionHelper;
use DateTimeImmutable;
use Faker\Generator as FakerGenerator;
use ReflectionException;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class CreatedAtModifierStrategy implements ModifierStrategyInterface
{
    private DateTimeImmutable $createdAt;

    /**
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(DateTimeImmutable $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @inheritDoc
     * @throws ReflectionException
     */
    public function apply(EntityInterface $entity, FakerGenerator $faker): void
    {
        ReflectionHelper::setPrivatePropertyValue(
            $entity,
            'createdAt',
            $this->createdAt
        );
    }
}
