<?php

declare(strict_types=1);

namespace App\Tests\Helper\Generator\User;

use App\Model\Base\Entity\EntityInterface;
use App\Model\User\Enum\UserRoleEnum;
use App\Tests\Helper\Generator\ModifierStrategyInterface;
use App\Tests\Helper\ReflectionHelper;
use Faker\Generator as FakerGenerator;
use ReflectionException;

/**
 * @link https://redmine.book24.ru/issues/
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class RolesModifierStrategy implements ModifierStrategyInterface
{
    /**
     * @var UserRoleEnum[]
     */
    private array $roles;

    /**
     * @param UserRoleEnum[] $roles
     */
    public function __construct(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * @inheritDoc
     * @throws ReflectionException
     */
    public function apply(EntityInterface $entity, FakerGenerator $faker): void
    {
        ReflectionHelper::setPrivatePropertyValue(
            $entity,
            'roles',
            $this->roles
        );
    }
}
