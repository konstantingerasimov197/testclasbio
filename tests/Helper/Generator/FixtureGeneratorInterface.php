<?php

declare(strict_types=1);

namespace App\Tests\Helper\Generator;

use App\Model\Base\Entity\EntityIdInterface;
use App\Model\Base\Entity\EntityInterface;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
interface FixtureGeneratorInterface
{
    /**
     * Генерация сущности
     *
     * @param GeneratorStrategyInterface $generatorStrategy
     * @param ModifierStrategyInterface[] $modifierStrategies
     *
     * @return EntityInterface
     */
    public function generate(
        GeneratorStrategyInterface $generatorStrategy,
        array $modifierStrategies
    ): EntityInterface;

    /**
     * Очистка внутреннего кэша
     */
    public function clear(): void;

    /**
     * Получение всех созданных сущностей указанного класса
     *
     * @param string $entityClass
     *
     * @return EntityInterface[]
     */
    public function getEntities(string $entityClass): array;

    /**
     * Получение созданной сущности
     *
     * @param string $entityClass
     * @param int $index
     *
     * @return EntityInterface
     */
    public function getEntity(string $entityClass, int $index): EntityInterface;

    /**
     * Получение идентификатора созданной сущности
     *
     * @param string $entityClass
     * @param int $index
     *
     * @return EntityIdInterface
     */
    public function getEntityId(string $entityClass, int $index): EntityIdInterface;
}
