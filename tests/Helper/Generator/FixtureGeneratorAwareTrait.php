<?php

declare(strict_types=1);

namespace App\Tests\Helper\Generator;

use App\Model\Base\Entity\EntityInterface;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
trait FixtureGeneratorAwareTrait
{
    /**
     * @var EntityInterface[]
     */
    protected array $entities = [];
    protected FixtureGeneratorInterface $generator;

    /**
     * @inheritDoc
     */
    public function setFixtureGenerator(FixtureGeneratorInterface $generator): void
    {
        $this->generator = $generator;
    }

    /**
     * @return FixtureGeneratorInterface
     * @deprecated Нужно использовать методы getEntity и getEntityId
     */
    public function getGenerator(): FixtureGeneratorInterface
    {
        return $this->generator;
    }
}
