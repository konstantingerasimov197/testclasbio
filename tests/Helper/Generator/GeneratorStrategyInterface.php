<?php

declare(strict_types=1);

namespace App\Tests\Helper\Generator;

use App\Model\Base\Entity\EntityInterface;
use Faker\Generator as FakerGenerator;

/**
 * Интерефейс для стратегий генераций сущностей
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
interface GeneratorStrategyInterface
{
    /**
     * Применение стратегии генерации сущности
     *
     * @param FakerGenerator $faker
     *
     * @return EntityInterface
     */
    public function generate(FakerGenerator $faker): EntityInterface;
}
