<?php

declare(strict_types=1);

namespace App\Tests\Assert;

use App\Tests\Assert\Response\IsHttpBadRequest;
use App\Tests\Assert\Response\IsHttpCreated;
use App\Tests\Assert\Response\IsHttpForbidden;
use App\Tests\Assert\Response\IsHttpNoContent;
use App\Tests\Assert\Response\IsHttpNotFound;
use App\Tests\Assert\Response\IsHttpOk;
use App\Tests\Assert\Response\IsHttpRedirect;
use App\Tests\Assert\Response\IsHttpUnauthorized;
use App\Tests\Assert\Response\IsHttpUnprocessableEntity;
use App\Tests\Assert\Response\IsJsonResponse;
use PHPUnit\Framework\Constraint\Constraint;
use Symfony\Component\HttpFoundation\Response;

/**
 * Трейт с кастомными ассертами
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 *
 * @method static assertThat($value, Constraint $constraint, string $message = ''): void
 */
trait WebAssert
{
    /**
     * @param Response $response
     * @param string $message
     */
    public function assertHttpOk(Response $response, $message = ''): void
    {
        self::assertThat($response, self::isHttpOk(), $message);
    }

    /**
     * @param Response $response
     * @param string $message
     */
    public function assertHttpCreated(Response $response, $message = ''): void
    {
        self::assertThat($response, self::isHttpCreated(), $message);
    }

    /**
     * @param Response $response
     * @param string $message
     */
    public function assertHttpNoContent(Response $response, $message = ''): void
    {
        self::assertThat($response, self::isHttpNoContent(), $message);
    }

    /**
     * @param Response $response
     * @param string $message
     */
    public function assertHttpBadRequest(Response $response, $message = ''): void
    {
        self::assertThat($response, self::isHttpBadRequest(), $message);
    }

    /**
     * @param Response $response
     * @param string $message
     */
    public function assertHttpUnauthorized(Response $response, $message = ''): void
    {
        self::assertThat($response, self::isHttpUnauthorized(), $message);
    }

    /**
     * @param Response $condition
     * @param string $message
     */
    public function assertHttpForbidden(Response $condition, $message = ''): void
    {
        self::assertThat($condition, self::isHttpForbidden(), $message);
    }

    /**
     * @param Response $condition
     * @param string $message
     */
    public function assertHttpNotFound(Response $condition, $message = ''): void
    {
        self::assertThat($condition, self::isHttpNotFound(), $message);
    }

    /**
     * @param Response $condition
     * @param string $message
     */
    public function assertHttpUnprocessableEntity(Response $condition, $message = ''): void
    {
        self::assertThat($condition, self::isHttpUnprocessableEntity(), $message);
    }

    /**
     * @param Response $condition
     * @param string $message
     */
    public function assertHttpRedirect(Response $condition, $message = ''): void
    {
        self::assertThat($condition, self::isHttpRedirect(), $message);
    }

    /**
     * @param Response $condition
     * @param string $message
     */
    public function assertJsonResponse(Response $condition, $message = ''): void
    {
        self::assertThat($condition, self::isJsonResponse(), $message);
    }

    /**
     * @return IsHttpOk
     */
    public static function isHttpOk(): IsHttpOk
    {
        return new IsHttpOk();
    }

    /**
     * @return IsHttpCreated
     */
    public static function isHttpCreated(): IsHttpCreated
    {
        return new IsHttpCreated();
    }

    /**
     * @return IsHttpNoContent
     */
    public static function isHttpNoContent(): IsHttpNoContent
    {
        return new IsHttpNoContent();
    }

    /**
     * @return IsHttpBadRequest
     */
    public static function isHttpBadRequest(): IsHttpBadRequest
    {
        return new IsHttpBadRequest();
    }

    /**
     * @return IsHttpUnauthorized
     */
    public static function isHttpUnauthorized(): IsHttpUnauthorized
    {
        return new IsHttpUnauthorized();
    }

    /**
     * @return IsHttpForbidden
     */
    public static function isHttpForbidden(): IsHttpForbidden
    {
        return new IsHttpForbidden();
    }

    /**
     * @return IsHttpNotFound
     */
    public static function isHttpNotFound(): IsHttpNotFound
    {
        return new IsHttpNotFound();
    }

    /**
     * @return IsHttpUnprocessableEntity
     */
    public static function isHttpUnprocessableEntity(): IsHttpUnprocessableEntity
    {
        return new IsHttpUnprocessableEntity();
    }

    /**
     * @return IsHttpRedirect
     */
    public static function isHttpRedirect(): IsHttpRedirect
    {
        return new IsHttpRedirect();
    }

    /**
     * @return IsJsonResponse
     */
    public static function isJsonResponse(): IsJsonResponse
    {
        return new IsJsonResponse();
    }
}
