<?php

declare(strict_types=1);

namespace App\Tests\Assert\Log;

use Monolog\Handler\TestHandler;
use PHPUnit\Framework\Constraint\Constraint;

/**
 * Базовый класс для ассертов для логов
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class BaseIsContainsLog extends Constraint
{
    protected TestHandler $handler;
    protected string $log;

    /**
     * @param TestHandler $handler
     */
    public function __construct(TestHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @inheritDoc
     *
     * @param string $log
     */
    protected function failureDescription($log): string
    {
        return sprintf('Log is not contains "%s"', $log);
    }

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return sprintf(
            'is contains log "%s"',
            $this->log
        );
    }
}
