<?php

declare(strict_types=1);

namespace App\Tests\Assert\Log;

/**
 * Проверка на наличие в логах нужного с типом INFO
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
final class IsContainsInfoLog extends BaseIsContainsLog
{
    /**
     * @inheritDoc
     *
     * @param string $log
     */
    public function matches($log): bool
    {
        $this->log = $log;

        return $this->handler->hasInfoThatContains($log);
    }
}
