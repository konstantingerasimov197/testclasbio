<?php

declare(strict_types=1);

namespace App\Tests\Assert;

use App\Tests\Assert\Message\IsMessageEquals;
use PHPUnit\Framework\Constraint\Constraint;
use Symfony\Component\Messenger\Envelope;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 *
 * @method static assertThat($value, Constraint $constraint, string $message = ''): void
 */
trait MessageAssert
{
    /**
     * @param string $expectedClass
     * @param array $expectedMessage
     * @param Envelope $envelope
     * @param string $message
     */
    public function assertMessageEquals(
        string $expectedClass,
        array $expectedMessage,
        Envelope $envelope,
        string $message = ''
    ): void {
        self::assertThat(
            $envelope,
            self::isMessageEquals($expectedClass, $expectedMessage),
            $message
        );
    }

    /**
     * @param string $expectedClass
     * @param array $expectedMessage
     *
     * @return IsMessageEquals
     */
    public static function isMessageEquals(string $expectedClass, array $expectedMessage): IsMessageEquals
    {
        return new IsMessageEquals($expectedClass, $expectedMessage);
    }
}
