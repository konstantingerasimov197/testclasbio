<?php

declare(strict_types=1);

namespace App\Tests\Assert\Message;

use PHPUnit\Framework\Constraint\Constraint;
use PHPUnit\Framework\ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;
use Symfony\Component\Messenger\Envelope;
use SebastianBergmann\Comparator\Factory as ComparatorFactory;

/**
 * Проверка на совпадение содержимого сообщения
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class IsMessageEquals extends Constraint
{
    private string $expectedClass;
    private array $expectedMessage;

    /**
     * @param string $expectedClass
     * @param array $expectedMessage
     */
    public function __construct(string $expectedClass, array $expectedMessage)
    {
        $this->expectedClass = $expectedClass;
        $this->expectedMessage = $expectedMessage;
    }

    /**
     * @inheritDoc
     *
     * @param Envelope $other
     */
    protected function matches($other): bool
    {
        $message = $other->getMessage();

        if (!$message instanceof $this->expectedClass) {
            return false;
        }

        $body = $message->serialize()->toArray();

        try {
            ComparatorFactory::getInstance()
                ->getComparatorFor($this->expectedMessage, $body)
                ->assertEquals($this->expectedMessage, $body);

            return true;
        } catch (ComparisonFailure $failure) {
            throw new ExpectationFailedException(
                trim($failure->getMessage()),
                $failure
            );
        }
    }

    /**
     * @inheritDoc
     *
     * @param Envelope $other
     */
    protected function failureDescription($other): string
    {
        return sprintf(
            'message are instase of %s',
            $this->expectedClass
        );
    }

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return 'is equals';
    }
}
