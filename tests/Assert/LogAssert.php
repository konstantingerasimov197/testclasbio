<?php

declare(strict_types=1);

namespace App\Tests\Assert;

use App\Tests\Assert\Log\IsContainsInfoLog;
use Monolog\Handler\TestHandler;
use PHPUnit\Framework\Constraint\Constraint;

/**
 * Трейт с кастомными ассертами
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 *
 * @method static assertThat($value, Constraint $constraint, string $message = ''): void
 */
trait LogAssert
{
    /**
     * @param TestHandler $handler
     * @param string $log
     * @param string $message
     */
    public function assertContainsInfoLog(TestHandler $handler, string $log, $message = ''): void
    {
        self::assertThat($log, self::isContainsInfoLog($handler), $message);
    }

    /**
     * @param TestHandler $handler
     *
     * @return IsContainsInfoLog
     */
    public static function isContainsInfoLog(TestHandler $handler): IsContainsInfoLog
    {
        return new IsContainsInfoLog($handler);
    }
}
