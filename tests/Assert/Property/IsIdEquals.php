<?php

declare(strict_types=1);

namespace App\Tests\Assert\Property;

use App\Model\Base\Entity\EntityIdInterface;
use PHPUnit\Framework\Constraint\Constraint;

/**
 * Проверка на эквивалентность идентификаторов сущностей
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class IsIdEquals extends Constraint
{
    private EntityIdInterface $expectedValue;

    /**
     * @param EntityIdInterface $expectedValue
     */
    public function __construct(EntityIdInterface $expectedValue)
    {
        $this->expectedValue = $expectedValue;
    }

    /**
     * @inheritDoc
     */
    protected function matches($other): bool
    {
        return $other->equals($this->expectedValue);
    }

    /**
     * @inheritDoc
     */
    protected function failureDescription($other): string
    {
        return sprintf(
            "two ids are equal\n%s\n%s\n@@ @@\n-%s\n+%s",
            '--- Expected',
            '+++ Actual',
            (string)$other->getId(),
            (string)$this->expectedValue->getId()
        );
    }

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return sprintf(
            'is "%s"',
            (string)$this->expectedValue->getId()
        );
    }
}
