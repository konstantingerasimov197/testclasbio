<?php

declare(strict_types=1);

namespace App\Tests\Assert\Property;

use MabeEnum\Enum;
use PHPUnit\Framework\Constraint\Constraint;

/**
 * Проверка Enum
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class IsEnumEquals extends Constraint
{
    /**
     * @var Enum|int|string|null
     */
    private $expectedValue;

    /**
     * @param Enum|int|string|null $expectedValue
     */
    public function __construct($expectedValue)
    {
        $this->expectedValue = $expectedValue;
    }

    /**
     * @inheritDoc
     */
    protected function matches($other): bool
    {
        return $other->is($this->expectedValue);
    }

    /**
     * @inheritDoc
     */
    protected function failureDescription($other): string
    {
        $actualValue = $other->getValue();
        $expectedValue = $this->expectedValue;

        if ($this->expectedValue instanceof Enum) {
            $expectedValue = $this->expectedValue->getValue();
        }

        if (is_string($actualValue)) {
            $actualValue = sprintf("'%s'", $actualValue);
        }

        if (is_string($expectedValue)) {
            $expectedValue = sprintf("'%s'", $expectedValue);
        }

        return sprintf(
            "two enums are equal\n%s\n%s\n@@ @@\n-%s\n+%s",
            '--- Expected',
            '+++ Actual',
            (string)$expectedValue,
            (string)$actualValue
        );
    }

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return sprintf(
            'is "%s"',
            $this->expectedValue
        );
    }
}
