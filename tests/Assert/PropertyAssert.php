<?php

declare(strict_types=1);

namespace App\Tests\Assert;

use App\Model\Base\Entity\EntityIdInterface;
use App\Tests\Assert\Property\IsEnumEquals;
use App\Tests\Assert\Property\IsIdEquals;
use MabeEnum\Enum;
use PHPUnit\Framework\Constraint\Constraint;

/**
 * Трейт с кастомными ассертами
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 *
 * @method static assertThat($value, Constraint $constraint, string $message = ''): void
 */
trait PropertyAssert
{
    /**
     * @param int|string|Enum|null $expectedValue
     * @param Enum $value
     * @param string $message
     */
    public function assertEnum($expectedValue, Enum $value, $message = ''): void
    {
        self::assertThat($value, self::isEnumEquals($expectedValue), $message);
    }

    /**
     * @param EntityIdInterface $expectedValue
     * @param EntityIdInterface $value
     * @param string $message
     */
    public function assertId(EntityIdInterface $expectedValue, EntityIdInterface $value, $message = ''): void
    {
        self::assertThat($value, self::isIdEquals($expectedValue), $message);
    }

    /**
     * @param $expectedValue
     *
     * @return IsEnumEquals
     */
    public static function isEnumEquals($expectedValue): IsEnumEquals
    {
        return new IsEnumEquals($expectedValue);
    }

    /**
     * @param EntityIdInterface $expectedValue
     *
     * @return IsIdEquals
     */
    public static function isIdEquals(EntityIdInterface $expectedValue): IsIdEquals
    {
        return new IsIdEquals($expectedValue);
    }
}
