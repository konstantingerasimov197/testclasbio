<?php

declare(strict_types=1);

namespace App\Tests\Assert\Response;

use PHPUnit\Framework\Constraint\Constraint;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class AbstractIsHttpCode extends Constraint
{
    protected int $expectedCode;

    /**
     * @param int $expectedCode
     */
    public function __construct(int $expectedCode)
    {
        $this->expectedCode = $expectedCode;
    }

    /**
     * @inheritDoc
     *
     * @param Response $response
     */
    public function evaluate($response, string $description = '', bool $returnResult = false): ?bool
    {
        parent::evaluate($response->getStatusCode());
        return null;
    }

    /**
     * @inheritDoc
     *
     * @param int $code
     */
    public function matches($code): bool
    {
        return $code === $this->expectedCode;
    }

    /**
     * @inheritDoc
     */
    protected function failureDescription($code): string
    {
        return 'HTTP code of response ' . $code . ' ' . $this->toString();
    }

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return sprintf(
            'is equal to %s',
            $this->expectedCode
        );
    }
}
