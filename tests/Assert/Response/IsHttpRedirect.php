<?php

declare(strict_types=1);

namespace App\Tests\Assert\Response;

use Symfony\Component\HttpFoundation\Response;

/**
 * Проверка на 302 код в ответе от сервера
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
final class IsHttpRedirect extends AbstractIsHttpCode
{
    public function __construct()
    {
        parent::__construct(Response::HTTP_FOUND);
    }
}
