<?php

namespace App\Tests;

use App\Model\Base\Entity\EntityIdInterface;
use App\Model\Base\Entity\EntityInterface;
use App\Tests\Assert\LogAssert;
use App\Tests\Assert\PropertyAssert;
use App\Tests\Helper\Generator\FixtureGenerator;
use App\Tests\Helper\Generator\FixtureGeneratorAwareInterface;
use App\Tests\Helper\Generator\FixtureGeneratorInterface;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\Persistence\ManagerRegistry;
use Exception;
use Monolog\Handler\TestHandler;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as BaseTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @author Oleg Maksimenko <ya@olegpro.ru>
 */
abstract class WebTestCase extends BaseTestCase
{
    use LogAssert;
    use PropertyAssert;

    private FixtureGeneratorInterface $fixtureGenerator;
    private array $fixtures = [];

    /**
     * @inheritDoc
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->fixtureGenerator = new FixtureGenerator();
    }

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        self::bootKernel();
        $fixtures = $this->getFixtures();

        if (!empty($fixtures)) {
            $this->loadFixtures($fixtures);
        }

        parent::setUp();
    }

    /**
     * @inheritDoc
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        $this->fixtures = [];
        $this->fixtureGenerator->clear();
    }

    /**
     * @inheritDoc
     */
    protected static function bootKernel(array $options = [])
    {
        if (!self::$booted) {
            parent::bootKernel($options);
        }

        return self::$kernel;
    }

    /**
     * @inheritDoc
     */
    protected static function createClient(array $options = [], array $server = [])
    {
        $kernel = static::bootKernel($options);
        $client = $kernel->getContainer()->get('test.client');
        $client->setServerParameters($server);

        return $client;
    }

    /**
     * @param array $fixtures
     * @param string $connectionManager
     */
    private function loadFixtures(array $fixtures, string $connectionManager = 'default'): void
    {
        $container = static::$container;
        $manager = $container->get(ManagerRegistry::class);
        $em = $manager->getManager($connectionManager);
        $loader = new Loader();

        foreach ($fixtures as $name => $class) {
            /** @var FixtureInterface $fixture */
            $fixture = new $class();

            if ($fixture instanceof FixtureGeneratorAwareInterface) {
                $fixture->setFixtureGenerator($this->fixtureGenerator);
            }

            $loader->addFixture($fixture);
            $this->fixtures[$name] = $fixture;
        }

        $executor = new ORMExecutor($em, new ORMPurger($em));
        $executor->execute($loader->getFixtures(), true);
    }

    /**
     * @return TestHandler
     */
    protected function getLogHandler(): TestHandler
    {
        /** @var TestHandler $logHandler */
        $logHandler = $this->getContainer()->get('monolog.handler.test');
        return $logHandler;
    }

    /**
     * @return ContainerInterface
     */
    protected function getContainer(): ContainerInterface
    {
        return static::$container;
    }

    /**
     * Возвращает путь к директории со вспомогательными ресурсами для тестирования
     *
     * @return string
     */
    protected function getTestDataPath(): string
    {
        return sprintf('%s/data', __DIR__);
    }

    /**
     * Возвращает список фикстур для базы даных проекта
     *
     * @return string[]
     */
    protected function getFixtures(): array
    {
        return [];
    }

    /**
     * @param string $entityClass
     * @param int $index
     *
     * @return EntityInterface
     * @throws Exception
     */
    protected function getEntity(string $entityClass, int $index): EntityInterface
    {
        return $this->fixtureGenerator->getEntity($entityClass, $index);
    }

    /**
     * @param string $entityClass
     * @param int $index
     *
     * @return EntityIdInterface
     * @throws Exception
     */
    protected function getEntityId(string $entityClass, int $index): EntityIdInterface
    {
        return $this->fixtureGenerator->getEntityId($entityClass, $index);
    }
}
