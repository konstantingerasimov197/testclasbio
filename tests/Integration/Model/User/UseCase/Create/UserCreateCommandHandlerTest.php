<?php

declare(strict_types=1);

namespace App\Tests\Integration\Model\User\UseCase\Create;

use App\Model\User\Enum\UserRoleEnum;
use App\Model\User\Repository\UserRepository;
use App\Model\User\UseCase\Create\UserCreateCommand;
use App\Tests\Integration\IntegrationTestCase;
use App\Tests\Integration\Model\User\UseCase\Create\Fixture\UserFixture;

/**
 * @testdox Тесты обработчика добавления пользователя
 * @group user
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserCreateCommandHandlerTest extends IntegrationTestCase
{
    /**
     * @inheritDoc
     */
    protected function getFixtures(): array
    {
        return [
            UserFixture::class,
        ];
    }

    /**
     * @testdox Добавление пользователя
     */
    public function testCreate(): void
    {
        $command = new UserCreateCommand(
            'new-user@test.test',
            '123456',
            [
                UserRoleEnum::admin()
            ]
        );

        $this->getCommandBus()->dispatch($command);

        /** @var UserRepository $repository */
        $repository = $this->getContainer()->get(UserRepository::class);

        $users = $repository->findBy([], ['createdAt' => 'DESC']);
        $this->assertCount(2, $users);

        $user = $users[0];
        $this->assertEquals('new-user@test.test', $user->getEmail());
        $this->assertNotNull($user->getPassword());

        $roles = $user->getRoles();
        $this->assertCount(1, $roles);

        $role = $roles[0];
        $this->assertEnum(UserRoleEnum::admin(), $role);
    }
}
