<?php

declare(strict_types=1);

namespace App\Tests\Integration\Model\User\UseCase\Create;

use App\Model\User\Entity\User;
use App\Model\User\Enum\UserRoleEnum;
use App\Model\User\UseCase\Create\UserCreateCommand;
use App\Tests\Integration\IntegrationTestCase;
use App\Tests\Integration\Model\User\UseCase\Create\Fixture\UserFixture;
use Exception;
use Symfony\Component\Messenger\Exception\ValidationFailedException;

/**
 * @testdox Тесты команды добавления пользователя
 * @group user
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserCreateCommandTest extends IntegrationTestCase
{
    /**
     * @inheritDoc
     */
    protected function getFixtures(): array
    {
        return [
            UserFixture::class,
        ];
    }

    /**
     * @testdox Дубликат Email
     *
     * @throws Exception
     */
    public function testEmailDuplicate(): void
    {
        $this->expectException(ValidationFailedException::class);

        /** @var User $user */
        $user = $this->getEntity(User::class, 0);

        $command = new UserCreateCommand(
            $user->getEmail(),
            '123456',
            [
                UserRoleEnum::admin()
            ]
        );

        $this->getCommandBus()->dispatch($command);
    }
}
