<?php

declare(strict_types=1);

namespace App\Tests\Integration\Model\User\UseCase\Delete;

use App\Model\User\UseCase\Delete\UserDeleteCommand;
use App\Model\User\Value\UserId;
use App\Tests\Integration\IntegrationTestCase;
use App\Tests\Integration\Model\User\UseCase\Delete\Fixture\UserFixture;
use Symfony\Component\Messenger\Exception\ValidationFailedException;

/**
 * @testdox Тесты команды удаления пользователя
 * @group user
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserDeleteCommandTest extends IntegrationTestCase
{
    /**
     * @inheritDoc
     */
    protected function getFixtures(): array
    {
        return [
            UserFixture::class,
        ];
    }

    /**
     * @testdox Пользователя не существует
     */
    public function testUserNotExists(): void
    {
        $this->expectException(ValidationFailedException::class);

        $command = new UserDeleteCommand(new UserId('not-exists'));
        $this->getCommandBus()->dispatch($command);
    }
}
