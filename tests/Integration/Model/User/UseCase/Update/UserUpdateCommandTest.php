<?php

declare(strict_types=1);

namespace App\Tests\Integration\Model\User\UseCase\Update;

use App\Model\User\Entity\User;
use App\Model\User\Enum\UserRoleEnum;
use App\Model\User\UseCase\Update\UserUpdateCommand;
use App\Model\User\Value\UserId;
use App\Tests\Integration\IntegrationTestCase;
use App\Tests\Integration\Model\User\UseCase\Update\Fixture\UserFixture;
use Exception;
use Symfony\Component\Messenger\Exception\ValidationFailedException;

/**
 * @testdox Тесты команды обновления пользователя
 * @group user
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserUpdateCommandTest extends IntegrationTestCase
{
    /**
     * @inheritDoc
     */
    protected function getFixtures(): array
    {
        return [
            UserFixture::class,
        ];
    }

    /**
     * @testdox Пользователя не существует
     *
     * @throws Exception
     */
    public function testUserNotExists(): void
    {
        $this->expectException(ValidationFailedException::class);

        $command = new UserUpdateCommand(
            new UserId('not-exists'),
            'new-user@clasbio.ru',
            null,
            [
                UserRoleEnum::admin(),
            ],
            true
        );

        $this->getCommandBus()->dispatch($command);
    }

    /**
     * @testdox Дубликат Email
     *
     * @throws Exception
     */
    public function testEmailDuplicate(): void
    {
        /** @var UserId $userId */
        $userId = $this->getEntityId(User::class, 0);

        $this->expectException(ValidationFailedException::class);

        $command = new UserUpdateCommand(
            $userId,
            'user@clasbio.ru',
            null,
            [
                UserRoleEnum::admin(),
            ],
            true
        );

        $this->getCommandBus()->dispatch($command);
    }
}
