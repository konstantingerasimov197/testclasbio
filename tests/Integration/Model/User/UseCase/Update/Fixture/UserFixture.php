<?php

declare(strict_types=1);

namespace App\Tests\Integration\Model\User\UseCase\Update\Fixture;

use App\Tests\Helper\Fixture\BaseFixture;
use App\Tests\Helper\Generator\User\CreatedAtModifierStrategy;
use App\Tests\Helper\Generator\User\EmailModifierStrategy;
use App\Tests\Helper\Generator\User\UserGeneratorStrategy;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserFixture extends BaseFixture
{
    /**
     * @inheritDoc
     */
    protected function internalLoad(EntityManagerInterface $manager): void
    {
        $this->addEntity(
            new UserGeneratorStrategy(),
            new CreatedAtModifierStrategy(
                new DateTimeImmutable('-1 days')
            )
        );

        $this->addEntity(
            new UserGeneratorStrategy(),
            new EmailModifierStrategy('user@clasbio.ru'),
            new CreatedAtModifierStrategy(
                new DateTimeImmutable('-1 days')
            )
        );
    }
}
