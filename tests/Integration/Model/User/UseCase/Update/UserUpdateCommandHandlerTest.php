<?php

declare(strict_types=1);

namespace App\Tests\Integration\Model\User\UseCase\Update;

use App\Model\User\Entity\User;
use App\Model\User\Enum\UserRoleEnum;
use App\Model\User\Repository\UserRepository;
use App\Model\User\UseCase\Update\UserUpdateCommand;
use App\Model\User\Value\UserId;
use App\Tests\Integration\IntegrationTestCase;
use App\Tests\Integration\Model\User\UseCase\Update\Fixture\UserFixture;
use Exception;

/**
 * @testdox Тесты обработчика обновления пользователя
 * @group user
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserUpdateCommandHandlerTest extends IntegrationTestCase
{
    /**
     * @inheritDoc
     */
    protected function getFixtures(): array
    {
        return [
            UserFixture::class,
        ];
    }

    /**
     * @testdox Редактирование пользователя
     *
     * @throws Exception
     */
    public function testUpdate(): void
    {
        /** @var UserId $userId */
        $userId = $this->getEntityId(User::class, 0);

        $command = new UserUpdateCommand(
            $userId,
            'updated@test.test',
            null,
            [
                UserRoleEnum::admin()
            ],
            false
        );

        $this->getCommandBus()->dispatch($command);

        /** @var UserRepository $repository */
        $repository = $this->getContainer()->get(UserRepository::class);

        $user = $repository->find($userId);
        $this->assertNotNull($user);
        $this->assertEquals('updated@test.test', $user->getEmail());
    }
}
