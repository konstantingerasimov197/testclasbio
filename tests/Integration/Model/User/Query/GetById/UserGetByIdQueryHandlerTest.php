<?php

declare(strict_types=1);

namespace App\Tests\Integration\Model\User\Query\GetById;

use App\Model\User\Entity\User;
use App\Model\User\Query\GetById\UserByIdQuery;
use App\Model\User\Query\GetById\UserByIdQueryResult;
use App\Model\User\Value\UserId;
use App\Tests\Integration\IntegrationTestCase;
use App\Tests\Integration\Model\User\Query\GetById\Fixture\UserFixture;
use Exception;

/**
 * @testdox Тесты обработчика получения пользователя по идентификатору
 * @user
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserGetByIdQueryHandlerTest extends IntegrationTestCase
{
    /**
     * @inheritDoc
     */
    protected function getFixtures(): array
    {
        return [
            UserFixture::class,
        ];
    }

    /**
     * @testdox Получение результата запроса
     *
     * @throws Exception
     */
    public function testInvoke(): void
    {
        /** @var UserId $userId */
        $userId = $this->getEntityId(User::class, 1);

        $query = new UserByIdQuery($userId);

        /** @var UserByIdQueryResult $result */
        $result = $this->getQueryBus()->query($query);
        $user = $result->getUser();

        $this->assertNotNull($user);
        $this->assertId($userId, $user->getId());
    }
}
