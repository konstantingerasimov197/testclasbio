<?php

declare(strict_types=1);

namespace App\Tests\Integration\Model\User\Query\Search\Fixture;

use App\Tests\Helper\Fixture\BaseFixture;
use App\Tests\Helper\Generator\User\UserGeneratorStrategy;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserFixture extends BaseFixture
{
    /**
     * @inheritDoc
     */
    protected function internalLoad(EntityManagerInterface $manager): void
    {
        $this->addEntity(
            new UserGeneratorStrategy()
        );

        $this->addEntity(
            new UserGeneratorStrategy()
        );
    }
}
