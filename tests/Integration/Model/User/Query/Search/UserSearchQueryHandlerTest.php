<?php

declare(strict_types=1);

namespace App\Tests\Integration\Model\User\Query\Search;

use App\Model\User\Entity\User;
use App\Model\User\Query\Search\UserSearchQuery;
use App\Model\User\Query\Search\UserSearchQueryResult;
use App\Tests\Integration\IntegrationTestCase;
use App\Tests\Integration\Model\User\Query\Search\Fixture\UserFixture;

/**
 * @testdox Тесты обработчка поиска списка пользователей с пагинацией
 * @group user
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class UserSearchQueryHandlerTest extends IntegrationTestCase
{
    /**
     * @inheritDoc
     */
    protected function getFixtures(): array
    {
        return [
            UserFixture::class,
        ];
    }

    /**
     * @testdox Получение данных
     */
    public function testInvoke(): void
    {
        $query = new UserSearchQuery(20, 0);

        /** @var UserSearchQueryResult $result */
        $result = $this->getQueryBus()->query($query);

        $list = $result->getList();
        $this->assertCount(2, $list);
        $this->assertContainsOnlyInstancesOf(User::class, $list);
        $this->assertEquals(2, $result->getCount());
    }
}
