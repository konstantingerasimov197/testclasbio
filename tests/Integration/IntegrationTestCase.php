<?php

namespace App\Tests\Integration;

use App\Infrastructure\MessageBus\Query\QueryBus;
use App\Tests\Assert\MessageAssert;
use App\Tests\WebTestCase;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Transport\InMemoryTransport;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class IntegrationTestCase extends WebTestCase
{
    use MessageAssert;

    /**
     * @return MessageBusInterface
     */
    public function getCommandBus(): MessageBusInterface
    {
        return $this->getContainer()
            ->get(MessageBusInterface::class);
    }

    /**
     * @return QueryBus
     */
    public function getQueryBus(): QueryBus
    {
        return $this->getContainer()
            ->get(QueryBus::class);
    }

    /**
     * @param string $name
     *
     * @return InMemoryTransport
     */
    public function getMessengerTransport(string $name): InMemoryTransport
    {
        return $this->getContainer()
            ->get('messenger.transport.' . $name);
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getEventDispatcher(): EventDispatcherInterface
    {
        return $this->getContainer()
            ->get(EventDispatcherInterface::class);
    }
}
