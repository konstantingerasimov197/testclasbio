<?php

declare(strict_types=1);

namespace App\Tests\Functional\Console;

use App\Tests\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Абстрактный класс для тестирования консольных команд
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class BaseCommandTest extends WebTestCase
{
    /**
     * @param string $name
     *
     * @return Command
     */
    protected function getConsoleCommand(string $name): Command
    {
        $application = new Application(static::$kernel);

        return $application->find($name);
    }

    /**
     * @param Command $command
     */
    protected function executeConsoleCommand(Command $command): void
    {
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'command' => $command->getName(),
            ],
            [
                'verbosity' => OutputInterface::VERBOSITY_VERBOSE,
            ]
        );
    }
}
