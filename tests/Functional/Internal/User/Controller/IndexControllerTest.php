<?php

declare(strict_types=1);

namespace App\Tests\Functional\Internal\User\Controller;

use App\Tests\Functional\BaseControllerTest;
use App\Tests\Functional\Internal\User\Controller\Fixture\UserFixture;
use Symfony\Component\HttpFoundation\Request;

/**
 * @testdox Тесты контроллера получения списка пользователей
 * @group user
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class IndexControllerTest extends BaseControllerTest
{
    private const URL = '/users';

    /**
     * @inheritDoc
     */
    protected function getFixtures(): array
    {
        return [
            UserFixture::class,
        ];
    }

    /**
     * @testdox Получение списка пользователей
     */
    public function testGet(): void
    {
        $client = $this->getApiClient();
        $client->request(Request::METHOD_GET, self::URL);

        $response = $client->getResponse();

        $this->assertHttpOk($response);
    }
}
