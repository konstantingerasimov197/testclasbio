<?php

declare(strict_types=1);

namespace App\Tests\Functional\Internal\User\Controller;

use App\Model\User\Entity\User;
use App\Tests\Functional\BaseControllerTest;
use App\Tests\Functional\Internal\User\Controller\Fixture\UserFixture;
use Exception;
use Symfony\Component\HttpFoundation\Request;

/**
 * @testdox Тесты контроллера обновления пользователя
 * @group user
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class ViewControllerTest extends BaseControllerTest
{
    private const URL = '/users/view/%s';

    /**
     * @inheritDoc
     */
    protected function getFixtures(): array
    {
        return [
            UserFixture::class,
        ];
    }

    /**
     * @testdox Пользователь не найден
     */
    public function testNotFound(): void
    {
        $client = $this->getApiClient();
        $client->request(Request::METHOD_GET, sprintf(self::URL, 'not-exists'));

        $response = $client->getResponse();
        $this->assertHttpNotFound($response);
    }

    /**
     * @testdox Получение страницы с данными
     *
     * @throws Exception
     */
    public function testGet(): void
    {
        $userId = $this->getEntityId(User::class, 0);

        $client = $this->getApiClient();
        $client->request(Request::METHOD_GET, sprintf(self::URL, $userId->getId()));

        $response = $client->getResponse();
        $this->assertHttpOk($response);
    }
}
