<?php

declare(strict_types=1);

namespace App\Tests\Functional\Internal\User\Controller;

use App\Model\User\Enum\UserRoleEnum;
use App\Tests\Functional\BaseControllerTest;
use App\Tests\Functional\Internal\User\Controller\Fixture\UserFixture;
use Symfony\Component\HttpFoundation\Request;

/**
 * @testdox Тесты контроллера добавления пользователя
 * @group user
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class CreateControllerTest extends BaseControllerTest
{
    private const URL = '/users/create';

    /**
     * @inheritDoc
     */
    protected function getFixtures(): array
    {
        return [
            UserFixture::class,
        ];
    }

    /**
     * @testdox Получение страницы с формой
     */
    public function testGet(): void
    {
        $client = $this->getApiClient();
        $client->request(Request::METHOD_GET, self::URL);

        $response = $client->getResponse();

        $this->assertHttpOk($response);
    }

    /**
     * @testdox Отправка формы
     */
    public function testPost(): void
    {
        $client = $this->getApiClient();
        $client->request(Request::METHOD_POST, self::URL, [
            'email' => 'test@test.test',
            'password' => '123456',
            'role' => UserRoleEnum::ADMIN,
        ]);

        $response = $client->getResponse();

        $this->assertHttpRedirect($response);
    }
}
