<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Tests\Assert\WebAssert;
use App\Tests\WebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * Абстрактный класс для тестов контроллеров
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
abstract class BaseControllerTest extends WebTestCase
{
    use WebAssert;

    /**
     * @return KernelBrowser
     */
    protected function getApiClient(): KernelBrowser
    {
        return static::createClient([]);
    }
}
