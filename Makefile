.PHONY: tests
.PHONY: phpcs

up: docker-up

stop: docker-stop

init: docker-clean \
    docker-up \
    php-create-env \
    php-composer \
    php-migration \
    php-migration-test \
    php-fixtures

php-database: php-migration \
    php-fixtures

php-create-env:
	docker-compose exec php rm -f .env.local
	docker-compose exec php cp .env .env.local
	docker-compose exec php sed -i 's/localhost:5432/postgres:5432/' .env.local
	docker-compose exec php rm -f phpunit.xml
	docker-compose exec php cp phpunit.xml.dist phpunit.xml
	docker-compose exec php sed -i 's/localhost:5432/postgres:5432/' phpunit.xml

docker-clean:
	docker-compose down --remove-orphans

docker-up:
	docker-compose up --build -d

docker-stop:
	docker-compose stop

php-composer:
	docker-compose exec php composer install

php-migration:
	docker-compose exec php bin/console doctrine:database:drop --if-exists --force
	docker-compose exec php bin/console doctrine:database:create -n -vvv
	docker-compose exec php bin/console doctrine:migrations:migrate -n -vvv

php-migration-test:
	docker-compose exec php bin/console doctrine:database:drop --env=test --if-exists --force
	docker-compose exec php bin/console doctrine:database:create --env=test -n -vvv
	docker-compose exec php bin/console doctrine:migrations:migrate --env=test -n -vvv

php-fixtures:
	docker-compose exec php bin/console doctrine:fixtures:load -n -vvv

composer-update:
	docker-compose exec php composer update


tests:
	docker-compose exec php composer run tests

phpcs:
	docker-compose exec php composer run phpcs --timeout 5000

phpcs-summary:
	docker-compose exec php composer run phpcs-summary --timeout 5000

phplint:
	docker-compose exec php composer run phplint

phpstan:
	docker-compose exec php composer run phpstan
