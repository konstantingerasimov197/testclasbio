<?php

declare(strict_types=1);

namespace PhpCs\Custom\Sniffs\Commenting;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Standards\PEAR\Sniffs\Commenting\FunctionCommentSniff as PearFunctionCommentSniff;

/**
 * Доработанное правило проверки наличия комментария у функции
 *
 * @author Alexander Kornilov <kornilov.alexand@gmail.com>
 */
class FunctionCommentSniff extends PearFunctionCommentSniff
{
    private const IGNORE_NAMES = [
        '__construct',
        '__destruct',
    ];

    /**
     * @inheritDoc
     */
    public function process(File $phpcsFile, $stackPtr): void
    {
        $methodName = $phpcsFile->getDeclarationName($stackPtr);

        if (in_array($methodName, self::IGNORE_NAMES, true)) {
            $params = $phpcsFile->getMethodParameters($stackPtr);

            if (empty($params)) {
                return;
            }
        }

        parent::process($phpcsFile, $stackPtr);
    }
}
